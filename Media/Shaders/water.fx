//--------------------------------------------------------------//
// First-step of water shader                                   //
//--------------------------------------------------------------//
float4x4 view_proj_matrix;
float4x4 view_matrix;
float4x4 matr;
float4x4 proj;
float4x4 Offset;
float4 viewPos;

float timer : TIME;
float4 Light_Attenuation = float4(0.001, 0.001, 0.001, 0.001);

texture Texture;
texture UnderWater;
texture Bump;
//--------------------------------------------------------------//
// Samplers
//--------------------------------------------------------------//
sampler Texture0 = sampler_state
{
   Texture   = <Texture>;
   ADDRESSU  = CLAMP;
   ADDRESSV  = CLAMP;
   MAGFILTER = LINEAR;
   MINFILTER = LINEAR;
};

sampler Under = sampler_state
{
   Texture = <UnderWater>;
   MAGFILTER = LINEAR;
   MINFILTER = LINEAR;
};

sampler BumpMap = sampler_state
{
   Texture = <Bump>;
   MAGFILTER = ANISOTROPIC;
   MINFILTER = ANISOTROPIC;
};

struct VS_OUTPUT 
{
   float4 Pos:      POSITION;
   float4 Tex0:     TEXCOORD0;
   float4 TexBump:  TEXCOORD1;
   float3 View   :  TEXCOORD2;
   float3 Light  :  TEXCOORD3;
};


VS_OUTPUT main( float4 inPos: POSITION,
                float4 inTex0: TEXCOORD0 )
{
   VS_OUTPUT Out;
   Out.TexBump = inTex0;
   // This shader pretty unsimilar standart pipeline's one )
   Out.Pos  = mul(inPos, view_proj_matrix);

   // Projection for full-screen texture
   Out.Tex0 = Out.Pos;
   Out.Tex0.y =- Out.Tex0.y;
   Out.Tex0.w *= 2;
   
   // Determine the distance from the light to the vertex and the direction
   float3 LightDir;
   LightDir.xyz = normalize(float3(1000*cos(timer/10), 550*sin(timer/10), 0));
   Out.Light = -LightDir;
    // Compute position in view space:
   float3 Pview = mul( inPos, view_matrix );
   // Compute the view direction in view space:
   Out.View = - normalize( Pview ); 
   return Out;
}

float4 ps_main( float4 inDiffuse: COLOR0,
                float4 inTex0:    TEXCOORD0,
                float4 inTexBump: TEXCOORD1,
                float3 View:      TEXCOORD2,
                float3 Light:     TEXCOORD3) : COLOR0
{
	float4 bumpMove = inTexBump;
	float wavespeed = 0.04;
	bumpMove.x += wavespeed * timer;
	bumpMove.y += cos(wavespeed * timer);
	bumpMove.z += cos(10*wavespeed*timer);
	float4 texOffset = tex3D(BumpMap,  1.15 * bumpMove);
	inTex0.xyz += texOffset.xyz;
	//������������ ��� ��������������� ������ ���� ���
    //float4 refl = lerp (tex2D( Texture0, float2(0.5,0.5)+inTex0.xy/inTex0.w ),
    //                   tex2D( Under, float2(0.5,0.5)+inTex0.xy/inTex0.w ), 0.35); 
	float4 refl = tex2D( Texture0, float2(0.5,0.5)+inTex0.xy/inTex0.w );
    
	// Compute the reflection vector:
	float3 Normal = normalize( mul( texOffset.xyz, view_matrix ) );
    float3 vReflect = normalize( 2 * dot( Normal, Light) * Normal - Light );       

    float4 SunColor = float4(saturate(sin(timer/10)+0.7), saturate(sin(timer/10.0)+0.6), saturate(sin(timer/10.0)+0.1)-0.25, 1);

	float4 SpecularColor = SunColor * pow( max( 0, dot(vReflect, View)), 7 );

    return refl*(0.7) + SpecularColor/2;
}

//--------------------------------------------------------------//
// Technique Section for water shader
//--------------------------------------------------------------//
technique Render
{
   pass Pass_0
   {
      VertexShader = compile vs_2_0 main();
      PixelShader = compile ps_2_0 ps_main();
   }

}