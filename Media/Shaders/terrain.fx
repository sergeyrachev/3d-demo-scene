float4x4 view_proj_matrix;
float4x4 world_view_matrix;
texture Texture;
texture Bump;
float4 Light_Attenuation = float4(0, 0, 0, 0);
float  ClPlane;
float timer;
//--------------------------------------------------------------//
// Samplers
//--------------------------------------------------------------//
sampler Texture0 = sampler_state
{
   Texture = <Texture>;
};
sampler bump = sampler_state
{
	Texture = <Bump>;
	MINFILTER = ANISOTROPIC;
	MAGFILTER = ANISOTROPIC;
};

struct VS_OUTPUT 
{
   float4 Pos:      POSITION;
   float2 Tex0:     TEXCOORD0;
   float4 Norm:     TEXCOORD1;
   float4 LightDir: TEXCOORD2; 

   // Extra
   float4 coord:   TEXCOORD3;
};

VS_OUTPUT vs_main( float4 inPos: POSITION,
                   float2 inTex0: TEXCOORD0,
                   float4 inNorm: NORMAL   )
{
   VS_OUTPUT Out;
   {
   	
   // Extra
   Out.coord = mul(inPos, world_view_matrix);

   //Pass the position, texcoord, normal
   Out.Pos  = mul(inPos, view_proj_matrix);
   Out.Tex0 = inTex0;
   Out.Norm = inNorm;

   //compute distance from "sun" to vertex
   float Dist = inPos.y - 10;
   Out.LightDir.xyz = normalize(float3(1000*cos(timer/10), 550*sin(timer/10), 0));
   // Compute the distance based attenuation. Distance can be interpolated
   // fairly well so we will precompute it on a per-vertex basis.
   Out.LightDir.w = Out.LightDir.y;
   
   }

   return Out;
}


//Function for computing per-pixel Diffuse lighting
float4 Light_PointDiffuse(float4 LightDir,
                                   float3 Normal,
                                   float4 LightColor)
{
// Compute suface/light angle based attenuation defined as dot(N,L)
float AngleAttn = clamp(0, 1, dot(Normal, LightDir.xyz) );
// Compute final lighting (Color * Distance Attenuation *
// Angle Attenuation)
return LightColor * AngleAttn;
}

float4 ps_main( float4 inDiffuse:   COLOR0,
                float2 inTex0:      TEXCOORD0,
                float4 inNorm:      TEXCOORD1,
                float4 inLightDir:  TEXCOORD2,
                float4 inPos: TEXCOORD3 ) : COLOR0
{
	float4 base = tex2D( Texture0, inTex0 );
	float4 Offset = 2 * tex2D( bump, inTex0*100) - 0.7;
	float4 Norm = normalize( inNorm + Offset);
	
	//compute dif color
	//float AngleAttn = clamp(0, 1, dot(Norm, inLightDir.xyz) );
	//float4 diffuse = float4(100, 100, 100, 100) * inLightDir.w * AngleAttn;

   // Extra
   clip(ClPlane * inPos.y/inPos.w);
   
   float4 SunColor = float4(saturate(sin(timer/10)+0.7), saturate(sin(timer/10.0)+0.6), saturate(sin(timer/10.0)+0.1)-0.25, 1);
   
   return base * (Light_PointDiffuse(inLightDir, Norm,float4(2, 2, 2, 2)) + SunColor);
}

//--------------------------------------------------------------//
// Technique Section for water shader
//--------------------------------------------------------------//
technique Render
{
   pass Pass_0
   {
      VertexShader = compile vs_2_0 vs_main();
      PixelShader = compile ps_2_0 ps_main();
   }

}