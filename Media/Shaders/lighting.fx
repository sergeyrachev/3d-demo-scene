float4x4 view_proj_matrix;
float4 Light_Attenuation = float4(0, 0, 0, 0);

// Base texture
texture Texture;

// Timer to get sun position
float timer;

//--------------------------------------------------------------//
// Samplers
//--------------------------------------------------------------//
sampler Texture0 = sampler_state
{
   Texture = <Texture>;
};

struct VS_OUTPUT 
{
   float4 Pos:      POSITION;
   float2 Tex0:     TEXCOORD0;
   float4 Norm:     TEXCOORD1;
   float4 LightDir: TEXCOORD2; 
};

VS_OUTPUT vs_main( float4 inPos: POSITION,
                   float2 inTex0: TEXCOORD0,
                   float4 inNorm: NORMAL )
{
   VS_OUTPUT Out;
   	
   //Pass the position, texcoord, normal
   Out.Pos  = mul(inPos, view_proj_matrix);
   Out.Tex0 = inTex0;
   Out.Norm = inNorm;

   Out.LightDir.xyz = normalize(float3(1000*cos(timer/10), 550*sin(timer/10), 0));
   Out.LightDir.w = 1;
   
   return Out;
}

//Function for computing per-pixel Diffuse lighting
float4 Light_PointDiffuse( float4 LightDir,
                           float3 Normal,
                           float4 LightColor )
{
   // Compute suface/light angle based attenuation defined as dot(N,L)
   float AngleAttn = max(0, dot(Normal, LightDir.xyz) );

   // Compute final lighting (Color * Distance Attenuation * Angle Attenuation)
   return LightColor * AngleAttn;
}

float4 ps_main( float4 inDiffuse:   COLOR0,
                float2 inTex0:      TEXCOORD0,
                float4 inNorm:      TEXCOORD1,
                float4 inLightDir:  TEXCOORD2 ) : COLOR0
{
   float4 base = tex2D( Texture0, inTex0 );
   float4 Norm = normalize( inNorm );

//   if( inLightDir.y<-0.15 ) inLightDir = float4( 0, 0, 0, 0 );
   return base * (Light_PointDiffuse(inLightDir, Norm,float4(2, 2, 2, 2))+inLightDir.y);
}

//--------------------------------------------------------------//
// Technique Section for shader
//--------------------------------------------------------------//
technique Render
{
   pass Pass_0
   {
      VertexShader = compile vs_2_0 vs_main();
      PixelShader = compile ps_2_0 ps_main();
   }
}