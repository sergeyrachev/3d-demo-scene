//--------------------------------------------------------------//	
// Dinamic texturing of SkySphere Effect //	
//--------------------------------------------------------------//	
float4x4 ViewProjMatrix : ViewProjection;	

texture1D   Sky;
texture3D   Noise;

float wind = 0.008;	
float timer = 0.0;	
float persistance = 2.0;	
float param1 = 1.0;
float param2 = 2.0;
float RadiusSphere = 1200;
float CoeffEllips  = 0.55;
float DelthaY;
float SunSlow = 10.0; //��� ������ ��� ������ ��������

sampler3D samplNoise = sampler_state	
{	
	Texture = (Noise);	
	MinFilter = LINEAR;	
	MagFilter = LINEAR;	
	MipFilter = LINEAR;	
};	

sampler1D samplSky = sampler_state
{
	Texture = (Sky);
	MinFilter = LINEAR;	
	MagFilter = LINEAR;	
	MipFilter = LINEAR;	
};	


struct VS_OUTPUT 	
{	
	float4 Pos: POSITION;	
	float2 TexNoise: TEXCOORD0;	
	float1 TexSky: TEXCOORD1;
	float FactorInterpolation: TEXCOORD3;
};	

VS_OUTPUT vs_main( float4 inPos: POSITION
				   ,float2 inTex0: TEXCOORD0
			       ,float1 inTex1: TEXCOORD1
				 )	
{	
	VS_OUTPUT Out;	
	Out.Pos = mul(inPos,ViewProjMatrix );
	
	float3 SunCoord = float3(RadiusSphere*cos(timer/SunSlow), RadiusSphere*CoeffEllips*sin(timer/SunSlow)+ DelthaY,0);
	
	
	//���������� ������� ��� ������������(�������� ������) ����������� �� ���������� �� ������ ������
	//�� ������� �������	
	Out.FactorInterpolation = length( mul(float4(SunCoord,1), ViewProjMatrix) - Out.Pos)/(0.2*length(SunCoord));	
		
	Out.TexNoise = inTex0.xy*1.8;
	Out.TexNoise.x +=timer*wind; 
	Out.TexSky = inTex1;

	return Out;	
}

float4 ps_main(	float3 inTex0: TEXCOORD0,
				float1 inTex1: TEXCOORD1,
				float factor: TEXCOORD3
			  ) : COLOR0	
{	
	float CloudDensity = 0;	
	float3 tex = float3( inTex0.xy, timer/200);
	for( int i=0; i<4; i++ )	
	{	
		CloudDensity += (1.0/pow(persistance, i))*(tex3D(samplNoise, tex*pow(2,i))*2-1);
	}	
	float SinTime = sin(timer/SunSlow);
	float4 SkyColor;
	SkyColor = tex1D(samplSky, inTex1);	
		
	// Filter clouds:)
	float CoeffFilter = 0.00002;
	CloudDensity = 1.0 - pow(CoeffFilter, CloudDensity);

	if( CloudDensity<0 ) CloudDensity=0;
	
	float4 SkyWithoutSun = (1-(1-((CloudDensity*(1-SkyColor.a) +1)/2))*(1-SkyColor));
	
	SkyWithoutSun.g *=saturate(SinTime+0.2);
				
	//������ +0.6, ����� ����� �������(��� ������ 0.6)  
	float4 SunColor =  float4(1, saturate(SinTime+0.6), saturate(SinTime+0.1)-0.25, 0);
	
	return lerp(SunColor,  SkyWithoutSun, (log(factor+0.1)));
}

technique Render	
{	
	pass P0	
	{ 	
	VertexShader = compile vs_2_0 vs_main( );		
	PixelShader = compile ps_2_0 ps_main( );	
	}	
}