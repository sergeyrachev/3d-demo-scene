#pragma once
// Exclude rarely-used stuff from windows header
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <mmsystem.h>
#pragma comment( lib, "winmm.lib" )

#include <d3d9.h>
#pragma comment( lib, "d3d9.lib" )
#include <d3dx9.h>
#pragma comment( lib, "d3dx9.lib" )

#include "CLog\CLog.h"


// Some useful macroses
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define SAFE_DELETE(x)       if(x) delete x;
#define SAFE_DELETE_ARRAY(x) if(x) delete[] x;
#define SAFE_RELEASE(x)      if(x) x->Release();

#define TRACE_ERROR( x ) { Logger.AddString(x); }
#define TRACE( x ) { Logger.AddMessage(x); }

// D3d vertex types definitions
struct COLORED_TEXTURED_VERTEX
{
    D3DXVECTOR3 position; // The position
    D3DCOLOR    color;    // The color
    FLOAT       tu, tv;   // The texture coordinates
	//add NS
	FLOAT		t2u,t2v;   // The second texture coordinates
	//////////////////////////////////////////////////////////////////////////
};

struct COLORED_TEXTURED_NORMAL_VERTEX
{
    D3DXVECTOR3 position; // The position
	D3DXVECTOR3 normal;   // The normal
    D3DCOLOR    color;    // The color
    FLOAT       tu, tv;   // The texture coordinates
};

struct COLORED_VERTEX
{
    D3DXVECTOR3 position; // The position
    D3DCOLOR    color;    // The color
};

// Our custom FVF, which describes our custom vertex structure

//replace NS
//#define D3DFVF_COLORED_TEXTURED_VERTEX (D3DFVF_XYZ|D3DFVF_DIFFUSE|D3DFVF_TEX1)
#define D3DFVF_COLORED_TEXTURED_VERTEX (D3DFVF_XYZ|D3DFVF_DIFFUSE|D3DFVF_TEX2)
//////////////////////////////////////////////////////////////////////////
#define D3DFVF_COLORED_VERTEX (D3DFVF_XYZ|D3DFVF_DIFFUSE)
#define D3DFVF_COLORED_TEXTURED_NORMAL_VERTEX (D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_DIFFUSE|D3DFVF_TEX1)