#pragma once
#include "CSceneObjectNode.h"
#include "CScene.h"
#include "..\StdAfx.h"

extern CLog Logger;

class CSkyBoxNode : public CSceneObjectNode
{
protected:
	WORD                    m_wYawParts;
	WORD                    m_wPitchParts;

	DWORD                   m_dwNumVerts;
	DWORD                   m_dwNumInds;

	DWORD                   m_dwRadius;

	int						m_dwDelthaY; // deltha y to up/down
	
	float					m_fCoeffEllips;
	float                   m_fAngle; // Bottom angle of Sphere (in rad)
		
	LPDIRECT3DTEXTURE9      m_pSkyTex;
	LPDIRECT3DVOLUMETEXTURE9 m_pNoiseTex;

	LPDIRECT3DVERTEXBUFFER9 m_pVB;
	LPDIRECT3DINDEXBUFFER9  m_pIB;
	
	ID3DXEffect*            m_pEffect;	
	CSceneNode*				m_camera;

public:
	CSkyBoxNode( CSceneNode* camera, const DWORD radius );
	~CSkyBoxNode();
	HRESULT VRestore  ( CScene* pScene );
	HRESULT VPreRender( CScene* pScene );
	HRESULT VRender   ( CScene* pScene );
	HRESULT VUpdate   ( CScene* pScene, const DWORD elapsedMs );
};