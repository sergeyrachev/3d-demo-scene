#pragma once
#include "CSceneObjectNode.h"
#include "CScene.h"

#include "..\StdAfx.h"
extern CLog Logger;

struct TRANSFORMED_VERTEX
{
	FLOAT x, y, z, rhw;   // Projected coordinates
	FLOAT       tu, tv;   // The texture coordinates
};

#define D3DFVF_TRANSFORMED_VERTEX ( D3DFVF_XYZRHW | D3DFVF_TEX1 )

class CWaterNode : public CSceneObjectNode
{
protected:
	LPDIRECT3DTEXTURE9      m_pTexture;
	LPDIRECT3DVERTEXBUFFER9 m_pVB;
	LPDIRECT3DINDEXBUFFER9	m_pIB;

	DWORD                   m_dwNumVerts;
	DWORD					m_dwNumPolys;
	DWORD                   m_dwWaterSize;
	const TCHAR*            m_texFile;
	const TCHAR*            m_fxFile;
	DWORD                   m_color;
	// Water shader
	ID3DXEffect*            m_pEffect;
	// HDR render target containing the scene
	LPDIRECT3DTEXTURE9      m_pTexScene;
	LPDIRECT3DTEXTURE9      m_pUnderWater;
	bool                    m_bRefl;
	IDirect3DSurface9      *m_pDepth;

	D3DXMATRIXA16           m_bump;//!!!FUCK AWAY
	LPDIRECT3DTEXTURE9	    m_pBumpTex;

	// Just for preview purposes!
	LPDIRECT3DVERTEXBUFFER9 m_pVB2;


public:
	CWaterNode( const DWORD WaterSize, LPDIRECT3DTEXTURE9 reflecttex, const D3DXMATRIX *m );
	~CWaterNode();
	HRESULT VRestore( CScene *pScene );
	HRESULT VRender ( CScene *pScene );
	//HRESULT VUpdate ( CScene *pScene, const DWORD elapsedMs);
};