#include "CInputController.h"

CInputController::CInputController( CSceneNode *object, D3DXMATRIXA16 *matPosition, D3DXMATRIXA16 *matToWorld, D3DXMATRIXA16 *matFromWorld )
: m_object(object)
{
	Logger.AddMessage("CInputController::CInputController()");

	m_fMouseSensitiveness = 0.3f;

	//D3DXMatrixIdentity( &m_matFromWorld );
    //D3DXMatrixIdentity( &m_matToWorld   );
	//D3DXMatrixIdentity( &m_matPosition  );
	m_matToWorld  = *matToWorld;
	m_matPosition = *matPosition;
	if( matFromWorld == NULL )
	{
	    D3DXMatrixInverse( &m_matFromWorld, NULL, &m_matToWorld );
	}

	m_fTargetYaw = m_fTargetPitch = 0.0f;
	m_fYaw = m_fPitch = 0.0f;

	m_bUserControl = false;

	memset( m_bKey, 0x00, sizeof(m_bKey) );
}

void CInputController::OnMouseMove( const POINT &mousePos )
{
	m_mouseClientPos = mousePos;

	if( m_bUserControl )
	{
        // Every time the mouse moves, figure out how far and in
        // which direction. The X axis is for yaw, the Y axis is
        // for pitch.
		//m_fTargetYaw   = m_fYawOnDown   + (m_mousePosOnDown.x - mousePos.x)*m_fMouseSensitiveness;
		//m_fTargetPitch = m_fPitchOnDown + (mousePos.y - m_mousePosOnDown.y)*m_fMouseSensitiveness;

		m_fTargetYaw   = m_fYaw   + (m_mouseClientPrevPos.x-m_mouseClientPos.x)*m_fMouseSensitiveness;
		m_fTargetPitch = m_fPitch + (m_mouseClientPos.y-m_mouseClientPrevPos.y)*m_fMouseSensitiveness;

//		m_fYaw   += (m_mouseClientPrevPos.x-m_mouseClientPos.x)*m_fMouseSensitiveness;
//		m_fPitch += (m_mouseClientPos.y-m_mouseClientPrevPos.y)*m_fMouseSensitiveness;
		
		//Logger.AddString( "m_mouseClientPrevPos: %d, %d\n", (m_mouseClientPosOnDown.x), (m_mouseClientPrevPos.y) );
		//Logger.AddString( "m_mouseClientPos: %d, %d\n", (m_mouseClientPos.x), (m_mouseClientPos.y) );

//		Logger.AddString( "mousedown: %d, %d\n", (m_mousePosOnDown.x), (m_mousePosOnDown.y) );
//		Logger.AddString( "mouse: %d, %d\n", (mousePos.x), (mousePos.y) );
//		SetCursorPos( m_mousePosOnDown.x, m_mousePosOnDown.y );
/*		POINT tmp;
		tmp.x = m_mousePosOnDown.x;
		tmp.y = m_mousePosOnDown.y;

		extern HWND hWnd;

		ClientToScreen( hWnd, &tmp );
		SetCursorPos( tmp.x, tmp.y );*/
//		a++;
	}

	m_mouseClientPrevPos = m_mouseClientPosOnDown;
	m_mouseClientPos = m_mouseClientPosOnDown;
}

void CInputController::OnLButtonDown( const POINT &mousePos )
{
    extern HWND hWnd;

	// The left button of mouse is down - record where it happened
	// and enable user control state
	m_bUserControl = true;
	m_mousePosOnDown = mousePos;
	m_mouseScreenPosOnDown = m_mouseClientPosOnDown = mousePos;
	ClientToScreen( hWnd, &m_mouseScreenPosOnDown );
	m_mouseClientPrevPos = mousePos;

	Logger.AddString( "MouseClientOnDown: %d, %d\n", (m_mouseClientPosOnDown.x), (m_mouseClientPosOnDown.y) );
	Logger.AddString( "MouseScreenOnDown: %d, %d\n", (m_mouseScreenPosOnDown.x), (m_mouseScreenPosOnDown.y) );

	m_fYawOnDown = m_fTargetYaw;
	m_fPitchOnDown = m_fTargetPitch;
}

void CInputController::OnRButtonUp( const POINT &mousePos )
{
    m_bUserControl = false;
}

void CInputController::Update( const DWORD elapsedMs )
{
	// Update angles of camera
    if( m_bUserControl )
	{
		SetCursorPos( m_mouseScreenPosOnDown.x, m_mouseScreenPosOnDown.y );

        // The secret formula!!! Don't give it away!
        m_fYaw += (m_fTargetYaw - m_fYaw) * ( 0.5f );
        m_fTargetPitch = MAX(-90, MIN(90, m_fTargetPitch));
        m_fPitch += (m_fTargetPitch - m_fPitch) * ( 0.5f );
	}

	// Update position
	if( m_bKey['W'] || m_bKey['S'] )
	{
        // In D3D, the "look at" default is always
        // the positive Z axis.
        D3DXVECTOR4 vLookAt = D3DXVECTOR4( 0.0f, 0.0f, 1.0f, 0.0f );
        if( m_bKey['S'] ) vLookAt *= -1;
        
		D3DXVECTOR4 atWorld( 0.0f, 0.0f, 0.0f, 0.0f );
        
		// This will give us the "look at" vector
        // in world space - we'll use that to move
        // the camera.        
		D3DXVec4Transform( &atWorld, &vLookAt, &m_matToWorld );

        // But not an entire 10 meters at a time!
        atWorld *= 0.01f*elapsedMs;

        D3DXMATRIX camTranslate;
        D3DXMatrixTranslation( &camTranslate, atWorld.x, atWorld.y, atWorld.z );
        
		// The newly created delta position matrix, camTranslate,
		// is concatenated with the member position matrix.
        D3DXMatrixMultiply(&m_matPosition, &m_matPosition, &camTranslate);
	}

	if( m_bKey['A'] || m_bKey['D'] )
	{
        // In D3D, the "look at" default is always
        // the positive Z axis. So right is positive
		// X axis.
        D3DXVECTOR4 vLookAt = D3DXVECTOR4( 1.0f, 0.0f, 0.0f, 0.0f );
        if( m_bKey['A'] ) vLookAt *= -1;
        
		D3DXVECTOR4 atWorld( 0.0f, 0.0f, 0.0f, 0.0f );
        
		// This will give us the "look at" vector
        // in world space - we'll use that to move
        // the camera.        
		D3DXVec4Transform( &atWorld, &vLookAt, &m_matToWorld );

        // But not an entire 10 meters at a time!
        atWorld *= 0.01f*elapsedMs;

        D3DXMATRIX camTranslate;
        D3DXMatrixTranslation( &camTranslate, atWorld.x, atWorld.y, atWorld.z );
        
		// The newly created delta position matrix, camTranslate,
		// is concatenated with the member position matrix.
        D3DXMatrixMultiply(&m_matPosition, &m_matPosition, &camTranslate);
	}

    // Calculate the new rotation matrix from the camera
    // yaw and pitch.
    D3DXMATRIX matRot;
    D3DXMatrixRotationYawPitchRoll( &matRot,
		-m_fYaw * D3DX_PI / 180,             // yaw   (x)
        m_fPitch * D3DX_PI / 180,            // pitch (y)
        0 );                                 // roll  (z)

	// Create the new object-to-world matrix, and the
    // new world-to-object matrix.
    D3DXMatrixMultiply( &m_matToWorld, &matRot, &m_matPosition );
	
	// Setup new matrix for assigned object
    D3DXMatrixInverse( &m_matFromWorld, NULL, &m_matToWorld );

	m_object->VSetTransform( &m_matToWorld, &m_matFromWorld );
}