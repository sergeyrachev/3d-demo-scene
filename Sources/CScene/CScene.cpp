#include "CScene.h"

CScene::CScene( IDirect3DDevice9 *device, CSceneNode *root )
: m_pInputController(NULL), m_root(root), m_bWireframe(false)
{
	m_pDevice = device;
	m_bRefl = false;
	D3DXCreateMatrixStack( 0, &m_matStack ); 
	Logger.AddMessage("CScene::CScene()");
}

CScene::~CScene()
{
	if( m_matStack!=NULL ) m_matStack->Release();
	Logger.AddMessage("CScene::~CScene()");
}

HRESULT CScene::Restore()
{
	// if there aren't children objects
	if( !m_root ) return S_OK;

	Logger.AddMessage("CScene::Restore()");

	return m_root->VRestore( this );
}

HRESULT CScene::Render()
{
	if( !m_root ) { return S_OK; }

	if( m_root->VPreRender( this ) == S_OK )
	{
		m_root->VRender( this );
		m_root->VRenderChildren( this );
		m_root->VPostRender( this );
	}

	return S_OK;
}

HRESULT CScene::Update()
{
	static DWORD lastTime = timeGetTime();
	DWORD elapsedTime = 0;
	DWORD curTime = timeGetTime();

	if( !m_root ) return S_OK;

	elapsedTime = curTime - lastTime;
	lastTime = curTime;

	if( m_pInputController ) m_pInputController->Update( elapsedTime );

	return m_root->VUpdate( this, elapsedTime );
}

void CScene::RenderEnv( LPDIRECT3DTEXTURE9 pRT)
{
		m_bRefl = true;

		// Extra var-s
		LPDIRECT3DSURFACE9 pSurfHDR; // High dynamic range surface to store
		pRT->GetSurfaceLevel( 0, &pSurfHDR );
		// Clear m_pTexScene
        m_pDevice->ColorFill( pSurfHDR, NULL, D3DCOLOR_ARGB(255, 0, 0, 175) );

		LPDIRECT3DSURFACE9 m_pDepth;
		m_pDevice->CreateDepthStencilSurface( 512, 512, D3DFMT_D24X8,
                                                    D3DMULTISAMPLE_NONE, 0,
			                                        TRUE, &m_pDepth, NULL );

		LPDIRECT3DSURFACE9 pSurfLDR; // Old render target
		m_pDevice->GetRenderTarget( 0, &pSurfLDR );

		LPDIRECT3DSURFACE9 pZBuffer; // Old Z-Buffer here
	    m_pDevice->GetDepthStencilSurface( &pZBuffer );

		// Change RT
		m_pDevice->SetRenderTarget( 0, pSurfHDR );
		m_pDevice->SetDepthStencilSurface( m_pDepth );
		m_pDevice->Clear( 0L, NULL, D3DCLEAR_ZBUFFER, 0x000000ff, 1.0f, 0L );

		Render();

		// Restore Settings
		m_pDevice->SetRenderTarget( 0, pSurfLDR );
		m_pDevice->SetDepthStencilSurface( pZBuffer );
		m_bRefl = false;

		SAFE_RELEASE(pSurfHDR);
		SAFE_RELEASE(pSurfLDR);
		SAFE_RELEASE(pZBuffer);
		SAFE_RELEASE(m_pDepth);
		// Extra lines-------------------------------------------------------------------------

}