#pragma once
#include <list>
#include <d3d9.h>
#include <d3dx9.h>

#include "..\CLog\Clog.h"
extern CLog Logger;


class CSceneNode;
class CScene;

typedef std::list<CSceneNode*> SceneNodeList;

class CSceneNode
{
public:
	SceneNodeList m_children;
	
	virtual ~CSceneNode();

    virtual HRESULT VRenderChildren( CScene* pScene );
	virtual HRESULT VRestore       ( CScene* pScene );
	virtual HRESULT VUpdate        ( CScene* pScene, const DWORD elapsedMs );

	virtual HRESULT VPreRender ( CScene* pScene ) { return S_OK; }
	virtual HRESULT VRender    ( CScene* pScene ) { return S_OK; }
	virtual HRESULT VPostRender( CScene* pScene ) { return S_OK; }

	virtual void VSetTransform( const D3DXMATRIX *toWorld, const D3DXMATRIX *fromWorld = NULL ) { }
};