#include "CCameraNode.h"

HRESULT CCameraNode::VUpdate( CScene *pScene, const DWORD elapsedMs )
{
	pScene->m_pDevice->SetTransform( D3DTS_VIEW, &m_matFromWorld );

	return S_OK;
}