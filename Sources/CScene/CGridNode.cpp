#include "CGridNode.h"

CGridNode::CGridNode( const DWORD gridSize, const DWORD color, const TCHAR* textureFile, const	D3DXMATRIX *m )
: CSceneObjectNode(m)
{
	Logger.AddMessage("CGridNode::CGridNode()");

	m_dwGridSize = gridSize;
	m_dwColor    = color;
	m_texFile    = textureFile;

	m_pTexture   = NULL;
	m_pVB        = NULL;
	m_pIB        = NULL;
	m_dwNumVerts = 0;
	m_dwNumPolys = 0;
}

CGridNode::~CGridNode()
{
	Logger.AddMessage("CGridNode::~CGridNode()");

	if( m_pTexture ) m_pTexture->Release();
	if( m_pVB ) m_pVB->Release();
	if( m_pIB ) m_pIB->Release();
}

HRESULT CGridNode::VRestore( CScene *pScene )
{
	Logger.AddMessage("CGridNode::VRestore()");

	// Call the base class's restore
	CSceneNode::VRestore( pScene );

	LPDIRECT3DDEVICE9 pDevice = pScene->m_pDevice;
	if( FAILED( D3DXCreateTextureFromFile( pDevice, const_cast<TCHAR*>(m_texFile), &m_pTexture ) ) )
		return E_FAIL;

    // Create the vertex buffer - we'll need enough verts
	// to populate the grid. If we want a 2x2 grid, we'll
	// need 3x3 set of verts.
	m_dwNumVerts = (m_dwGridSize+1)*(m_dwGridSize+1);

    if( FAILED( pDevice->CreateVertexBuffer( 
		m_dwNumVerts*sizeof(COLORED_TEXTURED_VERTEX),
        D3DUSAGE_WRITEONLY, D3DFVF_COLORED_TEXTURED_VERTEX,
        D3DPOOL_MANAGED, &m_pVB, NULL ) ) )
    {
        return E_FAIL;
    }

    // Fill the vertex buffer. We are setting the tu and tv texture
    // coordinates, which range from 0.0 to 1.0
    COLORED_TEXTURED_VERTEX* pVertices;
    if( FAILED( m_pVB->Lock( 0, 0, (void**)&pVertices, 0 ) ) )
        return E_FAIL;

	for( DWORD j=0; j<(m_dwGridSize+1); j++ )
    {
		for ( DWORD i=0; i<(m_dwGridSize+1); i++ )
		{
			// Which vertex are we setting?
			int index = i + (j * (m_dwGridSize+1) );
			COLORED_TEXTURED_VERTEX *vert = &pVertices[index];

			// Default position of the grid is at the origin, flat on
			// the XZ plane.
			float x = (float)i;
			float y = (float)j;
			vert->position = ( x * D3DXVECTOR3(10,0,0) ) + ( y * D3DXVECTOR3(0,0,10) );
			vert->color    = m_dwColor;

			// The texture coordinates are set to x,y to make the
			// texture tile along with units - 1.0, 2.0, 3.0, etc.
			vert->tu       = x;
			vert->tv       = y;
		}
	}
    m_pVB->Unlock();

	// The number of indicies equals the number of polygons times 3
	// since there are 3 indicies per polygon. Each grid square contains
	// two polygons. The indicies are 16 bit, since our grids won't
	// be that big!
	m_dwNumPolys = m_dwGridSize*m_dwGridSize*2;
	if( FAILED( pDevice->CreateIndexBuffer( sizeof(WORD) * m_dwNumPolys * 3,
				D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &m_pIB, NULL ) ) )
	{
		return E_FAIL;
	}

    WORD *pIndices;
    if( FAILED( m_pIB->Lock( 0, 0, (void**)&pIndices, 0 ) ) )
        return E_FAIL;
	
	// Loop through the grid squares and calc the values
	// of each index. Each grid square has two triangles:
	//
	//		A - B
	//		| / |
	//		C - D

	for( DWORD j=0; j<m_dwGridSize; j++ )
    {
		for ( DWORD i=0; i<m_dwGridSize; i++ )
		{
			// Triangle #1  ACB
			*(pIndices) = WORD(i + (j*(m_dwGridSize+1)));
			*(pIndices+1) = WORD(i + ((j+1)*(m_dwGridSize+1)));
			*(pIndices+2) = WORD((i+1) + (j*(m_dwGridSize+1)));

			// Triangle #2  BCD
			*(pIndices+3) = WORD((i+1) + (j*(m_dwGridSize+1)));
			*(pIndices+4) = WORD(i + ((j+1)*(m_dwGridSize+1)));
			*(pIndices+5) = WORD((i+1) + ((j+1)*(m_dwGridSize+1)));
			pIndices+=6;
		}
	}
	
    m_pIB->Unlock();
	return S_OK;
}

HRESULT CGridNode::VRender( CScene *pScene )
{
	//Logger.AddMessage("CGridNode::VRender()");

	LPDIRECT3DDEVICE9 pDevice = pScene->m_pDevice;

	DWORD oldLightMode;
	pDevice->GetRenderState( D3DRS_LIGHTING, &oldLightMode );
	pDevice->SetRenderState( D3DRS_LIGHTING, FALSE );

	DWORD oldCullMode;
	pDevice->GetRenderState( D3DRS_CULLMODE, &oldCullMode );
	pDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );

	// Setup our texture. Using textures introduces the texture stage states,
    // which govern how textures get blended together (in the case of multiple
    // textures) and lighting information. In this case, we are modulating
    // (blending) our texture with the diffuse color of the vertices.

	pDevice->SetTexture( 0, m_pTexture );
    pDevice->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE );
    pDevice->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
    pDevice->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );

	pDevice->SetStreamSource( 0, m_pVB, 0, sizeof(COLORED_TEXTURED_VERTEX) );
	pDevice->SetIndices( m_pIB );
	pDevice->SetFVF( D3DFVF_COLORED_TEXTURED_VERTEX );
	pDevice->DrawIndexedPrimitive( D3DPT_TRIANGLELIST , 0, 0, m_dwNumVerts, 0, m_dwNumPolys );

	// Notice that the render states are returned to 
	// their original settings.....
	// Could there be a better way???

	pDevice->SetTexture (0, NULL);
	pDevice->SetRenderState( D3DRS_LIGHTING, oldLightMode );
	pDevice->SetRenderState( D3DRS_CULLMODE, oldCullMode );

	return S_OK;
}