#pragma once
#include "CSceneObjectNode.h"
#include "CSceneNode.h"

#include "..\StdAfx.h"

extern CLog Logger;

class CScene;

class CCameraNode : public CSceneObjectNode
{
public:
	CCameraNode( const D3DXMATRIXA16 *t = NULL ) : CSceneObjectNode( (t==NULL)?D3DXMatrixIdentity(&m_matToWorld):t ) { Logger.AddMessage("CCameraNode::CCameraNode()"); };
	virtual HRESULT VUpdate( CScene *pScene, const DWORD elapsedMs );
};