#include "CSceneNode.h"

CSceneNode::~CSceneNode()
{
	Logger.AddMessage("CSceneNode::~CSceneNode()");

	while( !m_children.empty() )
	{
		m_children.pop_front();
	}
}

HRESULT CSceneNode::VRestore( CScene *pScene )
{
	SceneNodeList::iterator i = m_children.begin();

	while( i != m_children.end() )
	{
		(*i)->VRestore( pScene );
		i++;
	}

	return S_OK;
}

HRESULT CSceneNode::VUpdate( CScene *pScene, const DWORD elapsedMs )
{
	SceneNodeList::iterator i = m_children.begin();

	while( i != m_children.end() )
	{
		(*i)->VUpdate( pScene, elapsedMs );
		i++;
	}

	return S_OK;
}

HRESULT CSceneNode::VRenderChildren( CScene *pScene )
{
	SceneNodeList::iterator i = m_children.begin();

	while( i != m_children.end() )
	{
		if( (*i)->VPreRender( pScene )==S_OK )
		{
			(*i)->VRender( pScene );
			(*i)->VRenderChildren( pScene );
		}
		(*i)->VPostRender( pScene );		
		i++;
	}

	return S_OK;
}