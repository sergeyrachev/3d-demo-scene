#pragma once
#include "CSceneObjectNode.h"
#include "CScene.h"
#include "..\StdAfx.h"

extern CLog Logger;

class CMeshNode : public CSceneObjectNode
{
protected :
	D3DXMATRIX          m_matPosition;
	TCHAR              *m_pFileName;
	LPDIRECT3DTEXTURE9 *m_pTextures;
	LPD3DXMESH          m_pMesh;
	DWORD               m_dwNumMaterial;
	D3DMATERIAL9       *m_pMaterials;
	ID3DXEffect        *m_pEffect;		// Effect for simple perpixel lighting

public :
	CMeshNode( TCHAR* pFileName, const D3DXMATRIX *matPos );
	~CMeshNode();
	HRESULT VRender( CScene* pScene );
	HRESULT VUpdate( CScene* pScene, const DWORD elapsedMs );
	HRESULT VRestore( CScene* pScene );
};