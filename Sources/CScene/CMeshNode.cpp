#include "CMeshNode.h"

CMeshNode::CMeshNode(TCHAR* pFileName, const D3DXMATRIX *matPos)
: CSceneObjectNode( matPos )
{
	m_dwNumMaterial = 0;
	m_pMaterials = NULL;
	m_pMesh = NULL;
	m_pTextures = NULL;
	m_pEffect = NULL;
	m_pFileName = pFileName;
}

CMeshNode::~CMeshNode()
{
	SAFE_DELETE_ARRAY(m_pTextures);
	SAFE_RELEASE(m_pMesh);
	SAFE_DELETE_ARRAY(m_pMaterials);
	SAFE_RELEASE( m_pEffect );
}

HRESULT CMeshNode::VRestore( CScene *pScene )
{
	Logger.AddMessage("CMeshNode::VRestore()");

	LPDIRECT3DDEVICE9 pDevice = pScene->m_pDevice;

	// Call the base class's restore
	CSceneNode::VRestore( pScene );

	//Load effect
	if( FAILED( D3DXCreateEffectFromFile( pDevice, "Media/Shaders/lighting.fx", NULL, NULL, 0, NULL, &m_pEffect, NULL ) ) )
	{
		Logger.AddLine("Error. Can't create lighting effect\n");
		return E_FAIL;
	}

	LPD3DXBUFFER pD3DXMtrlBuffer;
	if( FAILED( D3DXLoadMeshFromX(m_pFileName, D3DXMESH_SYSTEMMEM, 
            pDevice, NULL, &pD3DXMtrlBuffer, NULL,
            &m_dwNumMaterial, &m_pMesh ) ) )
    {
		Logger.AddMessage("Loading Mesh from file .. FAILED");
		return E_FAIL;
	}
	

	D3DXMATERIAL* d3dxMaterials = (D3DXMATERIAL*)pD3DXMtrlBuffer->GetBufferPointer();

	m_pMaterials = new D3DMATERIAL9[m_dwNumMaterial];
	m_pTextures = new LPDIRECT3DTEXTURE9[m_dwNumMaterial];

	for(DWORD i=0; i<m_dwNumMaterial;i++)
	{
		m_pMaterials[i] = d3dxMaterials[i].MatD3D;

		m_pMaterials[i].Ambient = m_pMaterials[i].Diffuse;

		// Create the texture.
		
		if( FAILED( D3DXCreateTextureFromFile( pDevice, 
			d3dxMaterials[i].pTextureFilename, &m_pTextures[i] ) ) )
		{
			m_pTextures[i] = NULL;
		}
	}
	SAFE_RELEASE(pD3DXMtrlBuffer);
	
	return S_OK;
}

HRESULT CMeshNode::VRender( CScene* pScene )
{
	LPDIRECT3DDEVICE9 pDevice = pScene->m_pDevice;
	//pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME );
	
	// Effect Settings
	UINT iPass, cPasses;
	D3DXMATRIXA16 mWorld;
	D3DXMATRIXA16 mView;
	D3DXMATRIXA16 mProj;
	D3DXMATRIXA16 mWorldViewProjection;

	pDevice->GetTransform( D3DTS_WORLD, &mWorld );
	pDevice->GetTransform( D3DTS_VIEW, &mView );
	pDevice->GetTransform( D3DTS_PROJECTION, &mProj );
	mWorldViewProjection = mWorld * mView * mProj;

	m_pEffect->SetMatrix( "view_proj_matrix", &mWorldViewProjection );
	m_pEffect->SetFloat( "timer", timeGetTime()/500.0f );

	m_pEffect->SetTechnique( "Render" );

	m_pEffect->Begin( &cPasses, 0 );
	for( iPass = 0; iPass < cPasses; iPass++ )
	{
		m_pEffect->BeginPass(iPass);
		for( DWORD i=0; i < m_dwNumMaterial; i++ )
		{
			m_pEffect->SetTexture( "Texture", m_pTextures[i] );

			pDevice->SetTexture( 0, m_pTextures[i] );
			m_pMesh->DrawSubset( i );	
		}
		m_pEffect->EndPass();
	}
	m_pEffect->End();

	return S_OK;
}

HRESULT CMeshNode::VUpdate(CScene *pScene, const DWORD elapsedMs)
{
	return S_OK;
}