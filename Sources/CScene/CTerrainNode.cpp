#include "CTerrainNode.h"

CTerrainNode::CTerrainNode( const DWORD gridSize, const TCHAR* textureMap, const TCHAR* textureFile, const	D3DXMATRIX *m )
: CSceneObjectNode(m)
{
	Logger.AddMessage("CTerrainNode::CTerrainNode()");

	m_dwGridSize   = gridSize;
	m_dwColor      = 0xffffffff;
	m_texFile      = textureFile;
	m_texMap       = textureMap;

	m_pTexture     = NULL;
	m_pVB          = NULL;
	m_pIB          = NULL;
	m_pWireframeIB = NULL;
	m_pBump        = NULL;
	m_pEffect      = NULL;
	m_dwNumVerts   = 0;
	m_dwNumIndices = 0;
}

CTerrainNode::~CTerrainNode()
{
	Logger.AddMessage("CTerrainNode::~CTerrainNode()");

	SAFE_RELEASE( m_pTexture );
	SAFE_RELEASE( m_pVB );
	SAFE_RELEASE( m_pIB );
}

HRESULT CTerrainNode::VRestore( CScene *pScene )
{
	Logger.AddMessage("CTerrainNode::VRestore()");

	// Call the base class's restore
	CSceneNode::VRestore( pScene );

	LPDIRECT3DDEVICE9 pDevice = pScene->m_pDevice;
	LPDIRECT3DTEXTURE9 pHeightMap;

	// Save texture's parameters here
	D3DXIMAGE_INFO pTexInfo;

	if( FAILED( D3DXCreateTextureFromFileEx( pDevice, m_texMap,
											 D3DX_DEFAULT, D3DX_DEFAULT, // Size
											 D3DX_DEFAULT, 0,            // MipLevels, usage
											 D3DFMT_UNKNOWN,             // Format
											 D3DPOOL_MANAGED,            // Pool format
											 D3DX_FILTER_TRIANGLE,       // Filter
											 //D3DX_DEFAULT,               // Filter
											 //D3DX_FILTER_NONE,           // Filter
											 D3DX_DEFAULT,               // MipFilter
											 0,                          // Disable color key
											 &pTexInfo,                  // Information about texture
											 NULL,                       // 256-color pallete to fill in disable
											 &pHeightMap ) ) )
	{
		Logger.AddString( "Error. Can't find %s\n", m_texMap );	
		return E_FAIL;
	}

	// Some useful test-debug log info
	Logger.AddString( "Height map was loaded... Checking parameters...\n" );
	Logger.AddString( "Texture Size: %u x %u\n", pTexInfo.Width, pTexInfo.Height );
	Logger.AddString( "Texture depth: %u\n", pTexInfo.Depth );
	Logger.AddString( "Texture file format: %d\n", pTexInfo.ImageFileFormat );
	Logger.AddString( "Texture format: %u\n", pTexInfo.Format );

	m_wHeight = MIN( pTexInfo.Width, pTexInfo.Height );
	m_wWidth  = MAX( pTexInfo.Width, pTexInfo.Height );

	if( !(pTexInfo.Format==D3DFMT_A8 || pTexInfo.Format==D3DFMT_P8) )
	{
		TRACE_ERROR( "Unsupported format of Height map\n" );
		return E_FAIL;
	}

	// Lock the Height map for access
	D3DLOCKED_RECT d3dlrTex;
	pHeightMap->LockRect( 0, &d3dlrTex, NULL, 0 );
	// Use only 8-bit textures only, otherwise the created terrain will be incorrect!
	byte* pBits = (byte*)d3dlrTex.pBits;

	Logger.AddString( "Pitch bits: %d\n", d3dlrTex.Pitch );

	if( FAILED( D3DXCreateTextureFromFile( pDevice, const_cast<TCHAR*>(m_texFile), &m_pTexture ) ) )
		return E_FAIL;

	//Load effect
	if( FAILED( D3DXCreateEffectFromFile( pDevice, "Media/Shaders/terrain.fx", NULL, NULL, 0, NULL, &m_pEffect, NULL ) ) )
	{
		Logger.AddLine("Error. Can't create terrain effect\n");
		return E_FAIL;
	}

	//Load bumpmap
	if( FAILED( D3DXCreateTextureFromFile( pDevice, const_cast<TCHAR*>("Media/Textures/ocean.dds"), &m_pBump ) ) )
	{
		Logger.AddMessage( "Error. Can't load bump texture (terrain)\n" );
		return E_FAIL;
	}

    // Create and fill the vertex buffer - we'll need enough verts
	// to populate the terrain.
	m_dwNumVerts = m_wWidth*m_wHeight;

    if( FAILED( pDevice->CreateVertexBuffer( 
		m_dwNumVerts*sizeof(COLORED_TEXTURED_NORMAL_VERTEX),
        D3DUSAGE_WRITEONLY, D3DFVF_COLORED_TEXTURED_NORMAL_VERTEX,
        D3DPOOL_MANAGED, &m_pVB, NULL ) ) )
    {
		TRACE_ERROR( "Can't create vertex buffer\n" );
		return E_FAIL;
    }

	pDevice->CreateVertexBuffer( 
		m_dwNumVerts*2*sizeof(COLORED_VERTEX),
        D3DUSAGE_WRITEONLY, D3DFVF_COLORED_VERTEX,
        D3DPOOL_MANAGED, &m_pNormalVB, NULL ) ;

    COLORED_TEXTURED_NORMAL_VERTEX* pVertices;
    if( FAILED( m_pVB->Lock( 0, 0, (void**)&pVertices, 0 ) ) )
        return E_FAIL;
		
	COLORED_VERTEX* pVert;
    if( FAILED( m_pNormalVB->Lock( 0, 0, (void**)&pVert, 0 ) ) )
        return E_FAIL;

	DWORD i = 0;
	for( DWORD i1=0; i1<m_wHeight; i1++ )
    {
		for ( DWORD i2=0; i2<m_wWidth; i2++ )
		{
			pVertices[i].position = D3DXVECTOR3( static_cast<float>(i2*m_dwGridSize), 
												 static_cast<float>(pBits[i1*d3dlrTex.Pitch+i2])*m_dwGridSize/4.0f,
												 static_cast<float>(i1*m_dwGridSize) );
			pVertices[i].color    = m_dwColor;
			pVertices[i].normal   = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
			pVertices[i].tu       = (float)i2/(m_wWidth-1);
			pVertices[i].tv       = 1.0f - (float)i1/(m_wHeight-1);
//			Logger.AddString("x: %f, y: %f, color: %lu\n", x, y, pBits[j*256+i]);
			i++;
		}
	}

	pHeightMap->UnlockRect( 0 );
	
	SAFE_RELEASE( pHeightMap );

	// Create index buffer and fill it for the single stripe
	m_dwNumIndices = m_wWidth*2;
	if( FAILED( pDevice->CreateIndexBuffer( sizeof(WORD) * m_dwNumIndices,
				D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &m_pIB, NULL ) ) )
	{
		TRACE_ERROR( "Can't create index buffer\n" );
		return E_FAIL;
	}

    WORD *pIndices;
    if( FAILED( m_pIB->Lock( 0, 0, (void**)&pIndices, 0 ) ) )
        return E_FAIL;
	
	// Loop through the grid squares and calc the values
	// of each index. Each grid square has two triangles:
	//		A - B     so indices for the Triangle stripe
	//		| \ |     will be CADB
	//		C - D
	for( WORD i = 0; i<m_wWidth; i++ )
	{
		pIndices[i*2+0] = i + m_wWidth;
		pIndices[i*2+1] = i;
	}

	// Calculating normals
	for( DWORD i = 0; i<m_wHeight-1; i++ )
	{
		for( DWORD j=0; j<(m_wWidth-1)*2; j++ )
		{
			D3DXVECTOR3 normal;
			D3DXVec3Cross( &normal, 
				&(pVertices[i*m_wWidth+pIndices[j+0]].position-pVertices[i*m_wWidth+pIndices[j+1]].position),
				&(pVertices[i*m_wWidth+pIndices[j+2]].position-pVertices[i*m_wWidth+pIndices[j+1]].position) );

			if( j%2 ) normal =- normal;

			pVertices[i*m_wWidth+pIndices[j+0]].normal += normal;
			pVertices[i*m_wWidth+pIndices[j+1]].normal += normal;
			pVertices[i*m_wWidth+pIndices[j+2]].normal += normal;
		}
	}

	for( DWORD i=0; i<m_dwNumVerts; i++ )
	{
		D3DXVec3Normalize( &pVertices[i].normal, &pVertices[i].normal );

		// Debug info about normals
		pVert[2*i+0].position = pVertices[i].position;
		pVert[2*i+1].position = pVertices[i].position + 0.1*pVertices[i].normal;
		pVert[2*i+0].color    = 0x00ff0000;
		pVert[2*i+1].color    = 0x00ff0000;
	}

	m_pNormalVB->Unlock();
	m_pVB->Unlock();
	m_pIB->Unlock();

	return S_OK;
}


HRESULT CTerrainNode::VRender( CScene *pScene )
{
	LPDIRECT3DDEVICE9 pDevice = pScene->m_pDevice;

    pDevice->SetRenderState( D3DRS_LIGHTING, TRUE );

	D3DMATERIAL9 mtrl;
	ZeroMemory( &mtrl, sizeof(mtrl) );
	mtrl.Diffuse.r = mtrl.Ambient.r = 1.0f;
	mtrl.Diffuse.g = mtrl.Ambient.g = 1.0f;
	mtrl.Diffuse.b = mtrl.Ambient.b = 1.0f;
	mtrl.Diffuse.a = mtrl.Ambient.a = 1.0f;
	pDevice->SetMaterial( &mtrl );

	pDevice->SetTexture( 0, m_pTexture );
    pDevice->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE );    
	pDevice->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
    pDevice->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );

	// Don't draw unnecessary geometry
	pDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );

    // For Effect
	UINT iPass, cPasses;
	D3DXMATRIXA16 mWorld;
	D3DXMATRIXA16 mView;
	D3DXMATRIXA16 mProj;
	D3DXMATRIXA16 mWorldViewProjection;
	D3DXMATRIXA16 mWorldView;
    
	pDevice->GetTransform( D3DTS_WORLD, &mWorld );
	pDevice->GetTransform( D3DTS_VIEW, &mView );
	pDevice->GetTransform( D3DTS_PROJECTION, &mProj );
	mWorldViewProjection = mWorld * mView * mProj;
	mWorldView = mWorld * mView;

	m_pEffect->SetTexture( "Texture", m_pTexture);
	m_pEffect->SetTexture( "Bump", m_pBump);
	m_pEffect->SetMatrix( "view_proj_matrix", &mWorldViewProjection);
	m_pEffect->SetMatrix( "world_view_matrix", &mWorld );
	m_pEffect->SetFloat( "timer", timeGetTime()/500.0f );

	m_pEffect->SetTechnique( "Render" );

	pDevice->SetStreamSource( 0, m_pVB, 0, sizeof(COLORED_TEXTURED_NORMAL_VERTEX) );
	pDevice->SetIndices( m_pIB );
	pDevice->SetFVF( D3DFVF_COLORED_TEXTURED_NORMAL_VERTEX );
	
	//Determine if we are rendering reflection
	DWORD oldClip;
	pDevice->GetRenderState(D3DRS_CLIPPLANEENABLE, &oldClip);
	pDevice->SetRenderState(D3DRS_CLIPPLANEENABLE, 0);
	if(!oldClip)
	{
		m_pEffect->SetFloat("ClPlane", 0);
	}
	else m_pEffect->SetFloat("ClPlane", 1);
	
	m_pEffect->Begin( &cPasses, 0 );
		for( iPass = 0; iPass < cPasses; iPass++ )
		{
			m_pEffect->BeginPass(iPass);
			for( DWORD i=0; i<m_wHeight-1; i++ )
			{
				pDevice->DrawIndexedPrimitive( D3DPT_TRIANGLESTRIP, i*m_wWidth, 0, m_wWidth*2, 0, (m_wWidth-1)*2 );
			}
			m_pEffect->EndPass();
		}
		m_pEffect->End();

	if( pScene->m_bWireframe )
	{
		pDevice->SetTexture( 0, NULL );
		pDevice->SetRenderState( D3DRS_FILLMODE, D3DFILL_WIREFRAME );

		for( DWORD i=0; i<m_wHeight-1; i++ )
		{
			pDevice->DrawIndexedPrimitive( D3DPT_TRIANGLESTRIP, i*m_wWidth, 0, m_wWidth*2, 0, (m_wWidth-1)*2 );
		}

		pDevice->SetRenderState( D3DRS_FILLMODE, D3DFILL_SOLID );
	}

	if( 0==1 )
	{
        pDevice->SetRenderState( D3DRS_LIGHTING, FALSE );
		
		pDevice->SetStreamSource( 0, m_pNormalVB, 0, sizeof(COLORED_VERTEX) );
		pDevice->SetFVF( D3DFVF_COLORED_VERTEX );
		pDevice->DrawPrimitive( D3DPT_LINELIST, 0,  m_dwNumVerts );

        pDevice->SetRenderState( D3DRS_LIGHTING, TRUE );
	}

	return S_OK;
}