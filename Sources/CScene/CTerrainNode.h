#pragma once
#include "CSceneObjectNode.h"
#include "CScene.h"

#include "..\StdAfx.h"
extern CLog Logger;

class CTerrainNode : public CSceneObjectNode
{
protected:
	LPDIRECT3DTEXTURE9      m_pTexture;     // Terrain's texture
	LPDIRECT3DVERTEXBUFFER9 m_pVB;          // Vertex buffer
	LPDIRECT3DINDEXBUFFER9	m_pIB;          // Index buffer
	LPDIRECT3DINDEXBUFFER9	m_pWireframeIB; // Index buffer for wireframe mode
	DWORD                   m_dwNumVerts;
	DWORD					m_dwNumIndices;
	DWORD                   m_dwGridSize;
	DWORD                   m_dwColor;
	WORD                    m_wHeight;
	WORD                    m_wWidth;
	const TCHAR*            m_texFile;      // Terrain's texture file
	const TCHAR*            m_texMap;       // Height map file

	LPDIRECT3DVERTEXBUFFER9 m_pNormalVB;    // Extra buffer for normals line

	LPDIRECT3DTEXTURE9      m_pBump;        // Extra texture for bump
	ID3DXEffect            *m_pEffect;		// Effect for pixel lighting

public:
	CTerrainNode( const DWORD gridSize, const TCHAR* textureMap, const TCHAR* textureFile, const D3DXMATRIX *m );
	~CTerrainNode();
	HRESULT VRestore( CScene *pScene );
	HRESULT VRender ( CScene *pScene );

};