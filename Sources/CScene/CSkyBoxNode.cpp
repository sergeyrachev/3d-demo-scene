#include "CSkyBoxNode.h"


CSkyBoxNode::CSkyBoxNode( CSceneNode* camera, const DWORD radius )
: CSceneObjectNode( D3DXMatrixIdentity(&m_matToWorld) ), m_camera(camera)
{
	m_dwRadius = radius;
	m_fAngle = -30*D3DX_PI/180.0f;
	
	//Set pointer;
	m_pEffect = NULL;
	m_pSkyTex = NULL;
	m_pNoiseTex = NULL;
	m_dwDelthaY = 0;

	m_fCoeffEllips = static_cast<float>(0.55);

	// Level of Details
	m_wYawParts = 90;
	m_wPitchParts = 50;
	
	// Calculating parameters for current detalization
	m_dwNumVerts = (m_wYawParts+1)*(m_wPitchParts+1);
	m_dwNumInds  = m_wYawParts*(m_wPitchParts+1)+2;

}

CSkyBoxNode::~CSkyBoxNode()
{
	SAFE_RELEASE( m_pIB );
	SAFE_RELEASE( m_pVB );
	SAFE_RELEASE( m_pNoiseTex );
	SAFE_RELEASE( m_pSkyTex );
	SAFE_RELEASE( m_pEffect );
	SAFE_DELETE( m_camera );
//	Logger.AddMessage("CSkyBoxNode::~CSkyBoxNode() ... OK");
}

HRESULT CSkyBoxNode::VRestore( CScene* pScene )
{
	LPDIRECT3DDEVICE9 pDevice = pScene->m_pDevice;

	// Load sky texture
	if( FAILED( D3DXCreateTextureFromFile( pDevice, "Media/Textures/Sky.dds", &m_pSkyTex ) ) )
	{
		Logger.AddLine("Error. Can't load Sky texture file\n");
		return E_FAIL;
	}
	
	// Loading Noise Texture
	if( FAILED( D3DXCreateVolumeTextureFromFile( pDevice, "Media/Textures/NoiseVolume.dds", &m_pNoiseTex ) ) )
	{
		Logger.AddLine("Error. Can't Load noise texture file\n");
		return E_FAIL;
	}

	Logger.AddMessage("CSkyBoxNode::CSkyBoxNode() ... OK");

	// Assign pixel and vertex shaders
	if( FAILED( D3DXCreateEffectFromFile( pDevice, "Media/Shaders/sky.fx", NULL, NULL, 0, NULL, &m_pEffect, NULL ) ) )
	{
		Logger.AddLine("Error. Can't Load effect sky.fx\n");
		return E_FAIL;
	}

	
	// Create and fill vertex buffer
	if( FAILED( pDevice->CreateVertexBuffer( (m_dwNumVerts)*sizeof(COLORED_TEXTURED_VERTEX),
		D3DUSAGE_WRITEONLY, D3DFVF_COLORED_TEXTURED_VERTEX, D3DPOOL_MANAGED, &m_pVB, NULL ) ) )
	{
		Logger.AddLine( "Error. Can't create vertex buffer\n" );
		return E_FAIL;
	}
	
	COLORED_TEXTURED_VERTEX* pVertices;

    if( FAILED( m_pVB->Lock( 0, 0, (void**)&pVertices, 0 ) ) ){ return E_FAIL; }

	//Calculate coords of vertices
	DWORD i = 0, i1 = 0, i2 = 0;
	for( float theta = m_fAngle; i1<=m_wPitchParts; i1++, theta += (D3DX_PI/2-m_fAngle)/m_wPitchParts )
	{
		float r = m_dwRadius*cos(theta);
		for( float phi = 0, i2 = 0; i2<=m_wYawParts; i2++, phi += 2*D3DX_PI/m_wYawParts )
		{
			pVertices[i].position = D3DXVECTOR3( r*cosf(phi), m_dwRadius*m_fCoeffEllips*sinf(theta) + m_dwDelthaY, r*sin(phi) );
			pVertices[i].color = 0xffffffff;

			// Cylinder
			//add NS
			pVertices[i].t2v = (static_cast<float>(i2))/(m_wYawParts);
			pVertices[i].t2u = 1.0f-(static_cast<float>(i1))/(m_wPitchParts+1);
			////////////////////////////////////////////////////////
			// Top projection
			pVertices[i].tu = (pVertices[i].position.x/m_dwRadius/2.0f)+0.5f;
			pVertices[i].tv = (pVertices[i].position.z/m_dwRadius/2.0f)-0.5f;
			i++;
		}
	}
	m_pVB->Unlock();

	// Create and fill index buffer
	if( FAILED( pDevice->CreateIndexBuffer( sizeof(WORD)*m_dwNumInds,
		D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &m_pIB, NULL ) ) )
	{
		Logger.AddLine("Error. Can't create index buffer\n");
		return E_FAIL;
	}

	WORD* pIndices;
	
	if( FAILED( m_pIB->Lock( 0, 0, (void**)&pIndices, 0 ) ) ) { return E_FAIL; }

 	for( WORD i=0; i<=m_wYawParts; i++ )
	{
		pIndices[2*i+0] = (m_wYawParts+1)+i;
		pIndices[2*i+1] = i;
	}

	m_pIB->Unlock();

//	Logger.AddMessage("CSkyBoxNode::VRestore() ... OK");

	return S_OK;
}

HRESULT CSkyBoxNode::VPreRender( CScene* pScene )
{
	// Get camera's position matrix
	VSetTransform( pScene->m_pInputController->GetPositionMatrix() );

	return CSceneObjectNode::VPreRender( pScene );
}

HRESULT CSkyBoxNode::VRender( CScene* pScene )
{
	LPDIRECT3DDEVICE9 pDevice = pScene->m_pDevice;

	//HZ zachem
	/*pDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );
	pDevice->SetRenderState( D3DRS_LIGHTING, FALSE );
	DWORD oldClip;
	pDevice->GetRenderState( D3DRS_CLIPPLANEENABLE, &oldClip);
	pDevice->SetRenderState( D3DRS_CLIPPLANEENABLE, 0);*/

	pDevice->SetStreamSource( 0, m_pVB, 0, sizeof(COLORED_TEXTURED_VERTEX) );
	pDevice->SetFVF( D3DFVF_COLORED_TEXTURED_VERTEX );
	pDevice->SetIndices( m_pIB );

    UINT iPass, cPasses;
    D3DXMATRIXA16 mWorld;
    D3DXMATRIXA16 mView;
    D3DXMATRIXA16 mProj;
	D3DXMATRIXA16 mWorldViewProjection;

	//Preparing matrices for shaders
	{
	pDevice->GetTransform( D3DTS_WORLD, &mWorld );
	pDevice->GetTransform( D3DTS_VIEW, &mView );
	pDevice->GetTransform( D3DTS_PROJECTION, &mProj );
	mWorldViewProjection = mWorld * mView * mProj;
	}

	//Setting shader's parameters
	{
	m_pEffect->SetMatrix( "ViewProjMatrix", &mWorldViewProjection );
	m_pEffect->SetFloat( "timer", timeGetTime()/500.0f );
	m_pEffect->SetTexture( "Noise", m_pNoiseTex );
	m_pEffect->SetTexture( "Sky", m_pSkyTex );
	m_pEffect->SetTechnique( "Render" );
	m_pEffect->SetFloat( "RadiusSphere", static_cast<float>(m_dwRadius) );
	m_pEffect->SetFloat( "CoeffEllips", m_fCoeffEllips );
	m_pEffect->SetFloat( "DelthaY", static_cast<float>(m_dwDelthaY) );
	}
	
	// Begin drawing skysphere
	m_pEffect->Begin( &cPasses, 0 );
	for( iPass = 0; iPass < cPasses; iPass++ )
    {
		m_pEffect->BeginPass(iPass);
		for( DWORD i = 0; i<m_wPitchParts; i++ )
		{
			pDevice->DrawIndexedPrimitive( D3DPT_TRIANGLESTRIP, (m_wYawParts+1)*i, 0, m_dwNumVerts, 0, m_wYawParts*2 );
		}
		m_pEffect->EndPass();
	}
	m_pEffect->End();

	//Wireframe drawing
	if( pScene->m_bWireframe )
	{
		for( DWORD i = 0; i<m_wPitchParts; i++ )
		{
			pDevice->SetTexture( 0, NULL );
			pDevice->SetRenderState( D3DRS_FILLMODE, D3DFILL_WIREFRAME );
			pDevice->DrawIndexedPrimitive( D3DPT_TRIANGLESTRIP, (m_wYawParts+1)*i, 0, m_dwNumVerts, 0, m_wYawParts*2 );
		}
		pDevice->SetRenderState( D3DRS_FILLMODE, D3DFILL_SOLID );
	}
   
	//HZ zachem
	//pDevice->SetRenderState( D3DRS_LIGHTING, TRUE );
	//pDevice->SetRenderState( D3DRS_CLIPPLANEENABLE, oldClip);
	//Logger.AddMessage("CSkyBoxNode::VRender() ... OK");
	return S_OK;
}

HRESULT CSkyBoxNode::VUpdate( CScene* pScene, const DWORD elapsedMs )
{
	return S_OK;
}