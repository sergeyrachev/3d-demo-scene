#include "CSceneObjectNode.h"

HRESULT CSceneObjectNode::VPreRender( CScene* pScene )
{
	pScene->m_matStack->Push();
	pScene->m_matStack->MultMatrixLocal( &m_matToWorld );

	pScene->m_pDevice->SetTransform( D3DTS_WORLD, pScene->m_matStack->GetTop() );

	return S_OK;
}

HRESULT CSceneObjectNode::VPostRender( CScene* pScene )
{
	pScene->m_matStack->Pop();
	pScene->m_pDevice->SetTransform( D3DTS_WORLD, pScene->m_matStack->GetTop() );

	return S_OK;
}

void CSceneObjectNode::VSetTransform( const D3DXMATRIX *to, const D3DXMATRIX *from )
{
	m_matToWorld = *to;
	if( !from ) { D3DXMatrixInverse( &m_matFromWorld, NULL, &m_matToWorld ); }
	else { m_matFromWorld = *from; }
}