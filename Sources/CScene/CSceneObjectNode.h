#pragma once
#include "CScene.h"
#include "CSceneNode.h"

#include <d3d9.h>
#include <d3dx9.h>

#include "..\CLog\CLog.h"
extern CLog Logger;

class CSceneObjectNode : public CSceneNode
{
public:
	D3DXMATRIX m_matToWorld, m_matFromWorld;

	CSceneObjectNode( const D3DXMATRIX *to, const D3DXMATRIX *from = NULL ) { Logger.AddMessage("CSceneObjectNode::CSceneObjectNode()"); VSetTransform( to, from ); }
	
	virtual HRESULT VPreRender ( CScene* pScene );
	virtual HRESULT VPostRender( CScene* pScene );

	virtual void VSetTransform( const D3DXMATRIX *to, const D3DXMATRIX *from = NULL );
	virtual void VGetTransform() {};
};