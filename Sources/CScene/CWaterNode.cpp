#include "CWaterNode.h"

CWaterNode::CWaterNode( const DWORD WaterSize, LPDIRECT3DTEXTURE9 reflecttex, const D3DXMATRIX *m )
: CSceneObjectNode(m)
{
	m_dwWaterSize = WaterSize;
	//m_fxFile = fxFile;

	m_pTexture = reflecttex;
	if( m_pTexture ) m_pTexture->AddRef();
	m_pTexture = NULL;
	
	m_pVB        = NULL;
	m_pIB        = NULL;
	m_dwNumVerts = 0;
	m_dwNumPolys = 0;
	m_color = 0xff6699ff;
    //--------------------------------------------------------------------------------
	m_bRefl = false;
	m_pVB2  = NULL;
	//--------------------------------------------------------------------------------
	m_pEffect = NULL;
	D3DXMatrixIdentity( &m_bump);
	

	Logger.AddMessage("CWaterNode::CWaterNode()");
}

CWaterNode::~CWaterNode()
{
	Logger.AddMessage("CWaterNode::~CWaterNode()");

	if( m_pTexture ) m_pTexture->Release();
	if( m_pEffect ) m_pEffect->Release();
	if( m_pVB ) m_pVB->Release();
	if( m_pIB ) m_pIB->Release();
	SAFE_RELEASE( m_pEffect );
}

HRESULT CWaterNode::VRestore( CScene *pScene )
{
	Logger.AddMessage("CWaterNode::VRestore()");

	// Call the base class's restore
	CSceneNode::VRestore( pScene );

	LPDIRECT3DDEVICE9 pDevice = pScene->m_pDevice;

	// Create render target for water
    // Create the HDR scene texture
    if( FAILED( pDevice->CreateTexture(512, 512, 1, 
		                                D3DUSAGE_RENDERTARGET, D3DFMT_A16B16G16R16/*D3DFMT_X8R8G8B8*/, 
                                        D3DPOOL_DEFAULT, &m_pTexScene, NULL ) ) )
	{
		Logger.AddLine( "Error. Can't create HDR scene texture\n" );
		return E_FAIL;
	}
    if( FAILED( pDevice->CreateTexture(512, 512, 1, 
		                                D3DUSAGE_RENDERTARGET, D3DFMT_X8R8G8B8, 
                                        D3DPOOL_DEFAULT, &m_pUnderWater, NULL ) ) )
	{
		Logger.AddLine( "Error. Can't create underwater texture\n" );
		return E_FAIL;
	}
	
	// Clear HDR scene texture
    LPDIRECT3DSURFACE9 pSurface = NULL;
    HRESULT hr = m_pTexScene->GetSurfaceLevel( 0, &pSurface );
    if( SUCCEEDED(hr) )
        pDevice->ColorFill( pSurface, NULL, D3DCOLOR_ARGB(0, 0, 0, 215) );
    SAFE_RELEASE( pSurface );
	
	if( FAILED( D3DXCreateTextureFromFile( pDevice, const_cast<TCHAR*>("Media/Textures/water.jpg"), &m_pTexture ) ) )
	{
		Logger.AddMessage( "Error. Can't find texture file\n" );
		return E_FAIL;
	}

	// Create depth buffer:
	if( FAILED( pDevice->CreateDepthStencilSurface( 512, 512, D3DFMT_D24X8,
                                                    D3DMULTISAMPLE_NONE, 0,
			                                        TRUE, &m_pDepth, NULL ) ) )
	{
		Logger.AddMessage("Error in create stencil\n");
		return E_FAIL;
	}


	// Assign effect
	if( FAILED( D3DXCreateEffectFromFile( pDevice, const_cast<TCHAR*>("Media/Shaders/water.fx"), NULL, NULL, 0, NULL, &m_pEffect, NULL ) ) )
	{
		Logger.AddLine("Error. Can't find effect water.fx\n");
		return E_FAIL;
	}

    // Create the vertex buffer - we'll need enough verts
	// to populate the grid. If we want a 2x2 grid, we'll
	// need 3x3 set of verts.
	m_dwNumVerts = (m_dwWaterSize+1)*(m_dwWaterSize+1);

    if( FAILED( pDevice->CreateVertexBuffer( 
		m_dwNumVerts*sizeof(COLORED_TEXTURED_VERTEX),
        D3DUSAGE_WRITEONLY, D3DFVF_COLORED_TEXTURED_VERTEX,
        D3DPOOL_MANAGED, &m_pVB, NULL ) ) )
    {
        return E_FAIL;
    }

	//-------------------------------------------------------------------------------------
    if( FAILED( pDevice->CreateVertexBuffer( 
		4*sizeof(TRANSFORMED_VERTEX),
        D3DUSAGE_WRITEONLY, D3DFVF_TRANSFORMED_VERTEX,
        D3DPOOL_MANAGED, &m_pVB2, NULL ) ) )
    {
        return E_FAIL;
    }
	TRANSFORMED_VERTEX* pVerts;
    if( FAILED( m_pVB2->Lock( 0, 0, (void**)&pVerts, 0 ) ) )
        return E_FAIL;
	pVerts[0].x = 10.5;
	pVerts[0].y = 10.5;
	pVerts[0].z = 0;
	pVerts[0].rhw = 1;
	pVerts[0].tu = 0.0f;
	pVerts[0].tv = 0.0f;
	
	pVerts[1].x = 200.5;
	pVerts[1].y = 10.5;
	pVerts[1].z = 0;
	pVerts[1].rhw = 1;
	pVerts[1].tu = 1.0f;
	pVerts[1].tv = 0.0f;

	pVerts[3].x = 200.5;
	pVerts[3].y = 200.5;
	pVerts[3].z = 0;
	pVerts[3].rhw = 1;
	pVerts[3].tu = 1.0f;
	pVerts[3].tv = 1.0f;

	pVerts[2].x = 10.5;
	pVerts[2].y = 200.5;
	pVerts[2].z = 0;
	pVerts[2].rhw = 1;
	pVerts[2].tu = 0.0f;
	pVerts[2].tv = 1.0f;

    m_pVB2->Unlock();

	//-------------------------------------------------------------------------------------

    // Fill the vertex buffer. We are setting the tu and tv texture
    // coordinates, which range from 0.0 to 1.0
    COLORED_TEXTURED_VERTEX* pVertices;
    if( FAILED( m_pVB->Lock( 0, 0, (void**)&pVertices, 0 ) ) )
        return E_FAIL;

	for( DWORD j=0; j<(m_dwWaterSize+1); j++ )
    {
		for ( DWORD i=0; i<(m_dwWaterSize+1); i++ )
		{
			// Which vertex are we setting?
			int index = i + (j * (m_dwWaterSize+1) );
			COLORED_TEXTURED_VERTEX *vert = &pVertices[index];

			// Default position of the grid is at the origin, flat on
			// the XZ plane.
			float x = (float)i;
			float y = (float)j;
			vert->position = D3DXVECTOR3( 10.0f*x , 0.0f, 10.0f*y );
			vert->color = m_color;
			
			// The texture coordinates are set to x,y to make the
			// texture tile along with units - 1.0, 2.0, 3.0, etc.
			vert->tu = x;
			vert->tv = y;
		}
	}
    m_pVB->Unlock();

	// The number of indicies equals the number of polygons times 3
	// since there are 3 indicies per polygon. Each grid square contains
	// two polygons. The indicies are 16 bit, since our grids won't
	// be that big!
	m_dwNumPolys = m_dwWaterSize*m_dwWaterSize*2;
	if( FAILED( pDevice->CreateIndexBuffer( sizeof(WORD) * m_dwNumPolys * 3,
				D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &m_pIB, NULL ) ) )
	{
		return E_FAIL;
	}

    WORD *pIndices;
    if( FAILED( m_pIB->Lock( 0, 0, (void**)&pIndices, 0 ) ) )
        return E_FAIL;
	
	// Loop through the grid squares and calc the values
	// of each index. Each grid square has two triangles:
	//
	//		A - B
	//		| / |
	//		C - D

	for( DWORD j=0; j<m_dwWaterSize; j++ )
    {
		for ( DWORD i=0; i<m_dwWaterSize; i++ )
		{
			// Triangle #1  ACB
			*(pIndices) = WORD(i + (j*(m_dwWaterSize+1)));
			*(pIndices+1) = WORD(i + ((j+1)*(m_dwWaterSize+1)));
			*(pIndices+2) = WORD((i+1) + (j*(m_dwWaterSize+1)));

			// Triangle #2  BCD
			*(pIndices+3) = WORD((i+1) + (j*(m_dwWaterSize+1)));
			*(pIndices+4) = WORD(i + ((j+1)*(m_dwWaterSize+1)));
			*(pIndices+5) = WORD((i+1) + ((j+1)*(m_dwWaterSize+1)));
			pIndices+=6;
		}
	}
	
    m_pIB->Unlock();
	Logger.AddMessage("water restored");
	return S_OK;
}

HRESULT CWaterNode::VRender( CScene *pScene )
{
	//if( pScene->m_bWireframe ) m_bRefl = true;
	if( !pScene->m_bRefl )
	{
		// Extra lines-------------------------------------------------------------------------
		// Hmmmmmmmmmmmmmmmmmmmmmmmm..............................
		// Where's fakin' mistake??
		LPDIRECT3DDEVICE9 pDevice = pScene->m_pDevice;
		//rEFLECTION		

		D3DXMATRIXA16 mOldView;
		pDevice->GetTransform( D3DTS_VIEW, &mOldView );

		D3DXMATRIXA16 mOldProj;
		pDevice->GetTransform( D3DTS_PROJECTION, &mOldProj );

		D3DXMATRIXA16 mProjection;
	    D3DXMatrixPerspectiveFovLH( &mProjection, D3DX_PI/4, 1.33f, 1.0f, 1500.0f );
		pDevice->SetTransform( D3DTS_PROJECTION, &mProjection );

		D3DXMATRIXA16 mScale, mCam;
		D3DXMatrixScaling( &mScale, 1.0f, -1.0f, 1.0f );
		
		mCam = *pScene->m_pInputController->GetToWorldMatrix();
		mCam *= mScale; 

		D3DXMATRIXA16 mReflCam;
		D3DXMatrixInverse( &mReflCam, NULL, &mCam);

		// Update camera's info matrix
		pDevice->SetTransform( D3DTS_VIEW, &mReflCam );

		// Clipping
		D3DXPLANE ClipPlane;
		D3DXPlaneFromPoints( &ClipPlane, &D3DXVECTOR3( 0.0f, -0.2f, 0.0f ),
							 &D3DXVECTOR3( 0.0f, -0.2f, 1.0f ),
							 &D3DXVECTOR3( 1.0f, -0.2f, 1.0f ) );
	
		pDevice->SetClipPlane( 0, ClipPlane );
		pDevice->SetRenderState( D3DRS_CLIPPLANEENABLE, D3DCLIPPLANE0 );

		pScene->RenderEnv(m_pTexScene);

		// Restore Settings
		pDevice->SetTransform( D3DTS_VIEW, &mOldView );
		pDevice->SetRenderState( D3DRS_CLIPPLANEENABLE, 0 );
		
		//For rendering underwater
		//pScene->RenderEnv(m_pUnderWater);
		pDevice->SetTransform( D3DTS_PROJECTION, &mOldProj );

		

		// Extra lines-------------------------------------------------------------------------
		// Save settings
		DWORD oldLightMode;
		pDevice->GetRenderState( D3DRS_LIGHTING, &oldLightMode );
		pDevice->SetRenderState( D3DRS_LIGHTING, FALSE );

		DWORD oldCullMode;
		pDevice->GetRenderState( D3DRS_CULLMODE, &oldCullMode );
		pDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_CCW );

		DWORD oldWireframe;
		pDevice->GetRenderState( D3DRS_FILLMODE, &oldWireframe );
		// --end of save settings

		if( pScene->m_bWireframe ) pDevice->SetRenderState( D3DRS_FILLMODE, D3DFILL_WIREFRAME );
		pDevice->SetTexture( 0, m_pTexture );

		pDevice->SetStreamSource( 0, m_pVB, 0, sizeof(COLORED_TEXTURED_VERTEX) );
		pDevice->SetIndices( m_pIB );
		pDevice->SetFVF( D3DFVF_COLORED_TEXTURED_VERTEX );
		
		UINT iPass, cPasses;
		D3DXMATRIXA16 mWorld;
		D3DXMATRIXA16 mView;
		D3DXMATRIXA16 mProj;
		D3DXMATRIXA16 mWorldViewProjection;
		D3DXMATRIXA16 mWorldView;

		pDevice->GetTransform( D3DTS_WORLD, &mWorld );
		pDevice->GetTransform( D3DTS_VIEW, &mView );
		pDevice->GetTransform( D3DTS_PROJECTION, &mProj );
		mWorldViewProjection = mWorld * mView * mProj;

		mWorldView = mWorld * mView;

		D3DXVECTOR4 vViewPos = D3DXVECTOR4(mView._41, mView._42, mView._43, 0.0f);
		m_pEffect->SetVector( "viewPos", &vViewPos);

		m_pEffect->SetMatrix( "view_proj_matrix", &mWorldViewProjection );
		m_pEffect->SetMatrix( "view_matrix", &mWorldView );

		m_pEffect->SetMatrix( "proj", &mProj );

		m_pEffect->SetFloat( "timer", GetTickCount()/500.0f);

		m_pEffect->SetTexture( "Bump", m_pTexture );
		m_pEffect->SetTexture( "Texture", m_pTexScene );
		m_pEffect->SetTexture( "UnderWater", m_pUnderWater );

		m_pEffect->SetTechnique( "Render" );

		// Begin %)
		m_pEffect->Begin( &cPasses, 0 );
		for( iPass = 0; iPass < cPasses; iPass++ )
		{
			m_pEffect->BeginPass(iPass);

			pDevice->DrawIndexedPrimitive( D3DPT_TRIANGLELIST, 0, 0, m_dwNumVerts, 0, m_dwNumPolys );

			m_pEffect->EndPass();
		}
		m_pEffect->End();
		// End )

		pDevice->SetTexture( 0, m_pTexScene );
		pDevice->SetStreamSource( 0, m_pVB2, 0, sizeof(TRANSFORMED_VERTEX) );
		pDevice->SetFVF( D3DFVF_TRANSFORMED_VERTEX );
	//	pDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, 2 );

		// Restore settings
		pDevice->SetTexture( 0, NULL );
		pDevice->SetRenderState( D3DRS_LIGHTING, oldLightMode );
		pDevice->SetRenderState( D3DRS_CULLMODE, oldCullMode );
		pDevice->SetRenderState( D3DRS_FILLMODE, oldWireframe );
	}
	return S_OK;
}