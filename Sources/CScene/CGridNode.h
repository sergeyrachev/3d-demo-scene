#pragma once
#include "CSceneObjectNode.h"
#include "CScene.h"

#include "..\CLog\CLog.h"
extern CLog Logger;

class CGridNode : public CSceneObjectNode
{
protected:
	LPDIRECT3DTEXTURE9      m_pTexture;
	LPDIRECT3DVERTEXBUFFER9 m_pVB;
	LPDIRECT3DINDEXBUFFER9	m_pIB;
	DWORD                   m_dwNumVerts;
	DWORD					m_dwNumPolys;
	DWORD                   m_dwGridSize;
	DWORD                   m_dwColor;
	const TCHAR*            m_texFile;

public:
	CGridNode( const DWORD gridSize, const DWORD color, const TCHAR* textureFile, const	D3DXMATRIX *m );
	~CGridNode();
	HRESULT VRestore( CScene *pScene );
	HRESULT VRender ( CScene *pScene );
};