#pragma once
#include "CSceneNode.h"
#include "CInputController.h"
#include "d3dx9.h"
//#include "d3dx9math.h"

#include "..\CLog\Clog.h"
extern CLog Logger;

class CScene
{
public:
	IDirect3DDevice9  *m_pDevice;
	CSceneNode        *m_root;
	ID3DXMatrixStack  *m_matStack;
	CInputController  *m_pInputController;
	bool               m_bWireframe;
	bool			   m_bRefl;

	CScene( IDirect3DDevice9 *device, CSceneNode *root );
	~CScene();

	HRESULT Render();
	HRESULT Restore();
	HRESULT Update();
	void    RenderEnv( LPDIRECT3DTEXTURE9 pRT);

	void ToggleWireframe() { m_bWireframe = !m_bWireframe; }
};