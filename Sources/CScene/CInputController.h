#pragma once
#include "CSceneNode.h"
#include "..\StdAfx.h" 

extern CLog Logger;

// Mouse Interface
class IMouseSensitive
{
public:
	virtual void OnMouseMove  ( const POINT &mousePos ) = 0;
	virtual void OnLButtonDown( const POINT &mousePos ) = 0;
	virtual void OnLButtonUp  ( const POINT &mousePos ) = 0;
	virtual void OnRButtonDown( const POINT &mousePos ) = 0;
	virtual void OnRButtonUp  ( const POINT &mousePos ) = 0;
};

// Keyboard Interface
class IKeyboardSensitive
{
public:
	virtual void OnKeyDown( const byte c ) = 0;
	virtual void OnKeyUp  ( const byte c ) = 0;
};

class CInputController: public IMouseSensitive, public IKeyboardSensitive
{
protected:
	float m_fMouseSensitiveness;

	D3DXMATRIXA16 m_matPosition;
	D3DXMATRIXA16 m_matToWorld;
	D3DXMATRIXA16 m_matFromWorld;

	bool   m_bUserControl;
	POINT  m_mousePos;
	POINT  m_mousePosOnDown;
	POINT  m_mouseScreenPosOnDown;
	POINT  m_mouseClientPosOnDown;
	POINT  m_mouseClientPos;
	POINT  m_mouseClientPrevPos;

	BYTE   m_bKey[256];

	// Orientation of the camera
	float  m_fTargetYaw;
	float  m_fTargetPitch;
	float  m_fYaw;
	float  m_fPitch;
	float  m_fPitchOnDown;
	float  m_fYawOnDown;

	// Which object of scene will be controled?
	CSceneNode *m_object;

public:
	CInputController( CSceneNode *object, D3DXMATRIXA16 *matPosition, D3DXMATRIXA16 *matToWorld, D3DXMATRIXA16 *matFromWorld = NULL );
	void SetObject  ( CSceneNode *object );

	void Update( const DWORD elapsedMs );

	void OnMouseMove  ( const POINT &mousePos );
	void OnLButtonDown( const POINT &mousePos );
	void OnLButtonUp  ( const POINT &mousePos ) { /*m_bUserControl = false;*/ }
	void OnRButtonDown( const POINT &mousePos ) { }
	void OnRButtonUp  ( const POINT &mousePos );

	void OnKeyDown( const byte c ) { m_bKey[c] = true; }
	void OnKeyUp  ( const byte c ) { m_bKey[c] = false; }

	const D3DXMATRIXA16* GetToWorldMatrix()   { return &m_matToWorld; }
	const D3DXMATRIXA16* GetFromWorldMatrix() { return &m_matFromWorld; }
	const D3DXMATRIXA16* GetPositionMatrix()  { return &m_matPosition; }
};