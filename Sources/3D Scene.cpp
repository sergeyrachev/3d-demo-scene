#include "stdafx.h"
#include "CScene\CScene.h"

#include "CScene\CMeshNode.h"
#include "CScene\CSkyBoxNode.h"
#include "CScene\CWaterNode.h"
#include "CScene\CTerrainNode.h"
#include "CScene\CCameraNode.h"
#include "CScene\CGridNode.h"

//-----------------------------------------------------------------------------
// Global constants
//-----------------------------------------------------------------------------
char *gc_szAppTitle = "3D Scene"; // ��������� ����
int  gc_nWidth  = 800;            // ������ ����
int  gc_nHeight = 600;            // ������ ����

//-----------------------------------------------------------------------------
// Global variables
//-----------------------------------------------------------------------------
CLog Logger("3D Scene.log");                 // Log file
HWND hWnd;                                   // Window's descriptor
LPDIRECT3D9             g_pD3D       = NULL; // Used to create the D3DDevice
LPDIRECT3DDEVICE9       g_pd3dDevice = NULL; // Our rendering device

bool                    g_bCursor    = true;
CScene*                 g_pScene     = NULL; // Our Main Scene

struct CUSTOMVERTEX
{
    D3DXVECTOR3 position;
	D3DXVECTOR3 normal;
};

// Custom flexible vertex format (FVF)
#define D3DFVF_CUSTOMVERTEX ( D3DFVF_XYZ | D3DFVF_NORMAL )

//-----------------------------------------------------------------------------
// Forward definitions
//-----------------------------------------------------------------------------
LRESULT CALLBACK WndProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );
HWND CreateMainWindow( HINSTANCE hInstance, LPSTR lpcmdline, int ncmdshow );
HRESULT InitD3D( HWND hWnd );
void CreateScene();
void OnIdle();
void Cleanup();
void RenderScene();

//-----------------------------------------------------------------------------
// Name: InitD3D()
// Desc: Initializes Direct3D
//-----------------------------------------------------------------------------
HRESULT InitD3D( HWND hWnd )
{
    if( NULL == ( g_pD3D = Direct3DCreate9( D3D_SDK_VERSION ) ) ) return E_FAIL;

    D3DPRESENT_PARAMETERS d3dpp;
    ZeroMemory( &d3dpp, sizeof(d3dpp) );
    d3dpp.Windowed = true;
    d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
    d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
    // Z-buffer format
	d3dpp.EnableAutoDepthStencil = TRUE;
    d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
    // ������������ ������������� �������
    //d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_ONE;
    d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;

    if( FAILED( g_pD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
                                      D3DCREATE_HARDWARE_VERTEXPROCESSING,
                                      &d3dpp, &g_pd3dDevice ) ) )
    {
        Logger.AddMessage( "Can't create D3D Device" );
        return E_FAIL;
    }

	// Otherwise we cant see back side of triangle
    g_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );

	// Enable z-buffer
	g_pd3dDevice->SetRenderState( D3DRS_ZENABLE, TRUE );

	// Disable lighting
	g_pd3dDevice->SetRenderState( D3DRS_LIGHTING, FALSE );

	// Try to switch on anisotropic filtering
	g_pd3dDevice->SetSamplerState( 0, D3DSAMP_MAGFILTER, D3DTEXF_ANISOTROPIC );
	g_pd3dDevice->SetSamplerState( 0, D3DSAMP_MINFILTER, D3DTEXF_ANISOTROPIC );
	g_pd3dDevice->SetSamplerState( 0, D3DSAMP_MIPFILTER, D3DTEXF_ANISOTROPIC );
	g_pd3dDevice->SetSamplerState( 0, D3DSAMP_MAXANISOTROPY, 2 );


	D3DXMATRIX matProj;
    D3DXMatrixPerspectiveFovLH( &matProj, D3DX_PI/4, (float)gc_nWidth/gc_nHeight, 1.0f, 2500.0f );
	g_pd3dDevice->SetTransform( D3DTS_PROJECTION, &matProj );

/*    g_pd3dDevice->SetRenderState( D3DRS_LIGHTING, TRUE );
    g_pd3dDevice->SetRenderState( D3DRS_DITHERENABLE,   FALSE );
    g_pd3dDevice->SetRenderState( D3DRS_SPECULARENABLE, FALSE );
    g_pd3dDevice->SetRenderState( D3DRS_ZENABLE,        TRUE ); */
 //   g_pd3dDevice->SetRenderState( D3DRS_AMBIENT,        0x00fF000F );
/////////////////////////////////////////////////////////
    D3DXVECTOR3 vecDir;
    
	D3DLIGHT9 light;
    ZeroMemory( &light, sizeof(D3DLIGHT9) );
   /* light.Type       = D3DLIGHT_DIRECTIONAL;
    light.Ambient.r = light.Diffuse.r  = 1.0f;
    light.Ambient.g = light.Diffuse.g  = 1.0f;
    light.Ambient.b = light.Diffuse.b  = 1.0f;
    vecDir = D3DXVECTOR3( 0, -1.0f, 0 );
    D3DXVec3Normalize( (D3DXVECTOR3*)&light.Direction, &vecDir );
    light.Range       = 1.0f;*/

	light.Type = D3DLIGHT_SPOT;
	light.Ambient.r = light.Diffuse.r  = 1.0f;
    light.Ambient.g = light.Diffuse.g  = 1.0f;
    light.Ambient.b = light.Diffuse.b  = 1.0f;
	light.Position = D3DXVECTOR3(50.0f, 10.0f, 50.0f);
	vecDir = D3DXVECTOR3( 10.0f, -10.0f, 10.0f );
	D3DXVec3Normalize((D3DXVECTOR3*)&light.Direction, &vecDir );
	light.Range = 1000.0f;

	g_pd3dDevice->SetLight( 0 , &light );
    //g_pd3dDevice->LightEnable( 0, TRUE );
    g_pd3dDevice->SetRenderState( D3DRS_LIGHTING, TRUE );

    // Finally, turn on some ambient light.
    g_pd3dDevice->SetRenderState( D3DRS_AMBIENT, 0x50505050 );
    g_pd3dDevice->SetRenderState( D3DRS_AMBIENT, 0xffffffff );
/////////////////////////////////////////////////////////
	D3DXPLANE ClipPlane;
	D3DXPlaneFromPoints( &ClipPlane, &D3DXVECTOR3( 0.0f, -0.001f, 0.0f ),
						 &D3DXVECTOR3( 0.0f, -0.001f, 1.0f ),
						 &D3DXVECTOR3( 1.0f, -0.001f, 1.0f ) );
	
	g_pd3dDevice->SetClipPlane( 0, ClipPlane );
	//g_pd3dDevice->SetRenderState( D3DRS_CLIPPLANEENABLE, D3DCLIPPLANE0 );
/////////////////////////////////////////////////////////

    Logger.AddMessage("InitD3D() complete");

    return S_OK;
}


//-----------------------------------------------------------------------------
// Name: CreateScene()
// Desc: Creates our 3D scene
//-----------------------------------------------------------------------------
void CreateScene()
{
	// Useful identity matrix
	D3DXMATRIXA16 ident;
	D3DXMatrixIdentity( &ident );
	
	D3DXMATRIXA16 scale;
	D3DXMATRIXA16 trans;
	D3DXMATRIXA16 rot;

	// Root object (other objects will be placed relatively)
	CSceneNode* root = new CSceneObjectNode( &ident );

	// Camera node (abstract object)
	CSceneNode* camera = new CCameraNode( );
	root->m_children.push_back( camera );

	// Terrain (1 is Grid' Size, 14.0f - waterline)
	D3DXMatrixTranslation( &trans, 372.0f, -8.0f, 372.0f );
	CSceneNode* terra = new CTerrainNode( 1, "Media/Textures/map6.bmp", "Media/Textures/txtr6.bmp", &trans );
	root->m_children.push_back( terra );

	// Sky Sphere
	CSceneNode* sky = new CSkyBoxNode( camera, 1000 );
	root->m_children.push_back( sky );

	// Water
	D3DXMatrixTranslation( &trans, 0.0f, 0.0f, 0.0f );
	CSceneNode* water = new CWaterNode( 100, NULL, &trans );
	root->m_children.push_back( water );
	
	// Meshes
	// House
	D3DXMatrixRotationX( &rot, -D3DX_PI/2 );
	D3DXMatrixScaling( &scale, 0.01f, 0.01f, 0.01f );
	D3DXMatrixTranslation( &trans, 500.0f, 5.8f, 500.0f );
	trans = rot * scale * trans;
	CSceneNode* mesh = new CMeshNode( "Media/Meshes/House.x", &trans );
	root->m_children.push_back( mesh );

	//// Tree
	//D3DXMatrixRotationX( &rot, -D3DX_PI/2 );
	//D3DXMatrixScaling( &scale, 0.05f, 0.05f, 0.05f );
	//D3DXMatrixTranslation( &trans, 470.0f, 0.8f, 513.0f );
	//trans = rot * scale * trans;
	//CSceneNode* tree = new CMeshNode( "Media/Meshes/Tree.x", &trans );
	//root->m_children.push_back( tree );

	// Create main scene with assigned input controller
	g_pScene = new CScene( g_pd3dDevice, root );
	g_pScene->Restore();

	D3DXMatrixTranslation( &trans,500.0f, 7.0f, 500.0f );
	D3DXMatrixIdentity( &rot );
	CInputController* InputController = new CInputController( camera, &trans, &rot );

	g_pScene->m_pInputController = InputController;
}


//-----------------------------------------------------------------------------
// Name: RenderScene()
// Desc: Renders Objects of Scene
//-----------------------------------------------------------------------------
void RenderScene()
{
    // Calculating of FPS
	static int  NumFrames = 0;
	static long LastTime = timeGetTime();
    // When the whole second is ellapsed
	if( timeGetTime() - LastTime >= 1000 )
    {
        LastTime = timeGetTime();
        char NewCaption[22] = "\0";
        wsprintf( NewCaption, "%s [%u fps]", gc_szAppTitle, NumFrames );
		NumFrames = 0;
        SetWindowText( hWnd, NewCaption );
	}
	else NumFrames += 1;

	g_pd3dDevice->Clear( 0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0xff5e3789, 1.0f, 0 );
	if( SUCCEEDED( g_pd3dDevice->BeginScene() ) )
    {
		if( g_pScene ) g_pScene->Update(); // Animate first
		if( g_pScene ) g_pScene->Render(); // Render
		g_pd3dDevice->EndScene();
    }
	else Logger.AddMessage( "Error! Can't BeginScene()" );

	g_pd3dDevice->Present( NULL, NULL, NULL, NULL );
}


//-----------------------------------------------------------------------------
// Name: Cleanup()
// Desc: Releases all previously initialized objects
//-----------------------------------------------------------------------------
void Cleanup()
{
    Logger.AddMessage( "Cleanup()" );

	if( g_pScene != NULL )
	{
		Logger.AddLine("Main Scene released\n");
		SAFE_DELETE(g_pScene->m_pInputController);
		SAFE_DELETE(g_pScene->m_root);
		delete g_pScene;
	}

    if( g_pd3dDevice != NULL )
    {
		Logger.AddLine("D3D Device released\n");
        g_pd3dDevice->Release();
    }

    if( g_pD3D != NULL )
    {
		Logger.AddLine("D3D Interface released\n");
        g_pD3D->Release();
    }

    Logger.AddMessage( "Cleanup done" );
}


//-----------------------------------------------------------------------------
// Name: OnIdle()
// Desc: If there aren't messages in list, do some actions
//-----------------------------------------------------------------------------
void OnIdle()
{
    RenderScene();
}


//-----------------------------------------------------------------------------
// Name: WndProc()
// Desc: The window's message handler
//-----------------------------------------------------------------------------
LRESULT CALLBACK WndProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	POINT mouseClientPos;

    switch(msg)
    {
    case WM_KEYDOWN:
        switch(wParam)
        {
        case VK_ESCAPE:
            SendMessage( hWnd, WM_CLOSE, 0, 0 );
            break;
		default:
			if( g_pScene && g_pScene->m_pInputController )
				g_pScene->m_pInputController->OnKeyDown( static_cast<const byte>(wParam) );
			if( static_cast<const byte>(wParam)==static_cast<const byte>('Q')) g_pScene->ToggleWireframe();
			break;
        }// switch(wParam)
    break;

	case WM_SETCURSOR:
		if( !g_bCursor ) SetCursor( NULL );
		else SetCursor( LoadCursor( NULL, IDC_ARROW ) );
		break;

	case WM_KEYUP:
		if( g_pScene && g_pScene->m_pInputController )
			g_pScene->m_pInputController->OnKeyUp( static_cast<const byte>(wParam) );
		break;

	case WM_MOUSEMOVE:
		mouseClientPos.x = LOWORD(lParam);
		mouseClientPos.y = HIWORD(lParam);

		if( g_pScene && g_pScene->m_pInputController )
			g_pScene->m_pInputController->OnMouseMove( mouseClientPos );
		break;

	case WM_LBUTTONDOWN:
		SetCapture( hWnd );
		g_bCursor = false;
		SetCursor( NULL );

		mouseClientPos.x = LOWORD(lParam);
		mouseClientPos.y = HIWORD(lParam);

		if( g_pScene && g_pScene->m_pInputController )
			g_pScene->m_pInputController->OnLButtonDown( mouseClientPos );
		break;

	case WM_LBUTTONUP:
		SetCapture( NULL );

		mouseClientPos.x = LOWORD(lParam);
		mouseClientPos.y = HIWORD(lParam);

		if( g_pScene && g_pScene->m_pInputController )
			g_pScene->m_pInputController->OnLButtonUp( mouseClientPos );
		break;

	case WM_RBUTTONDOWN:
		SetCapture( hWnd );
		g_bCursor = true;

		mouseClientPos.x = LOWORD(lParam);
		mouseClientPos.y = HIWORD(lParam);

		if( g_pScene && g_pScene->m_pInputController )
			g_pScene->m_pInputController->OnRButtonDown( mouseClientPos );
		break;

	case WM_RBUTTONUP:
		SetCapture( NULL );

		mouseClientPos.x = LOWORD(lParam);
		mouseClientPos.y = HIWORD(lParam);

		if( g_pScene && g_pScene->m_pInputController )
			g_pScene->m_pInputController->OnRButtonUp( mouseClientPos );
		break;

    case WM_DESTROY:
        Cleanup();
        PostQuitMessage(0);
        break;

    default:
        return DefWindowProc( hWnd, msg, wParam, lParam );
    }

    return 0;
}


//-----------------------------------------------------------------------------
// Name: CreateMainWindow()
// Desc: Creates window of application. Size of work area is exactly
//       gc_nWidth by gc_Height pixels.
//-----------------------------------------------------------------------------
HWND CreateMainWindow( HINSTANCE hInstance, LPSTR lpcmdline, int ncmdshow )
{
	WNDCLASSEX WinClass;
    WinClass.cbSize        = sizeof(WNDCLASSEX);
    WinClass.style         = CS_HREDRAW | CS_VREDRAW;

    WinClass.lpfnWndProc   = WndProc;
    WinClass.cbClsExtra    = NULL;       // ���� �������������� ����������
    WinClass.cbWndExtra    = NULL;       // ������ �� ������������
    WinClass.hInstance     = hInstance;  // ���������� ���������� ����������
    WinClass.hIcon         = LoadIcon( NULL, IDI_APPLICATION );
    WinClass.hCursor       = LoadCursor( NULL, IDC_ARROW );
    WinClass.hbrBackground = (HBRUSH)GetStockObject(LTGRAY_BRUSH);
    WinClass.lpszMenuName  = NULL;
    WinClass.lpszClassName = "WinClass";
    WinClass.hIconSm       = LoadIcon( NULL, IDI_APPLICATION );

    if( !RegisterClassEx(&WinClass) )
    {
        Logger.AddString("Fatal Error. Can't register main window class!\n");
        return 0;
    }

	// Adjust window's work area
	RECT rc;
	SetRect( &rc, 0, 0, gc_nWidth, gc_nHeight );
	AdjustWindowRect( &rc, WS_OVERLAPPEDWINDOW, true );

	HWND hWnd;
    hWnd = CreateWindowEx(
        NULL,                 // �������������� ����� ����
        "WinClass",           // ��� ������
        gc_szAppTitle,        // ��������� ����
        WS_OVERLAPPEDWINDOW,  // �����
        (GetSystemMetrics(SM_CXSCREEN)-(rc.right-rc.left))/2, // ������� ����
        (GetSystemMetrics(SM_CYSCREEN)-(rc.bottom-rc.top))/2, // ���������� �� X � Y
		rc.right-rc.left,
		rc.bottom-rc.top,     // ��� ������
        NULL,                 // ��������
        NULL,                 // �������� ���� ��� ��������� �� ����
        hInstance,
        NULL);

    if( NULL==hWnd )
    {
        Logger.AddMessage("Fatal Error. Can't create main window");
        return NULL;
    }

    // ������ ���� ���� �������
    ShowWindow( hWnd, ncmdshow );
    UpdateWindow( hWnd );

    Logger.AddMessage("Main window created");

	return hWnd;
}


//-----------------------------------------------------------------------------
// Name: WinMain()
// Desc: Application's entry point
//-----------------------------------------------------------------------------
int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpcmdline, int ncmdshow )
{
	hWnd = CreateMainWindow( hInstance, lpcmdline, ncmdshow );
	if( hWnd == NULL ) return 0;
	// �������������� Direct3D
    InitD3D( hWnd );
    // Load Scene Geometry
    CreateScene();

    // ���� ��������� ���������
    MSG msg;
    while( TRUE )
    {
        if( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) )
        {
            // ��������� ��������� � ������
            if( msg.message == WM_QUIT ) break;
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
		else { OnIdle(); }
    }

	Logger.AddLine("\n\nExit.");
    return 0;
}