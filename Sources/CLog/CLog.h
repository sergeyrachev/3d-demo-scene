#ifndef __CLOG_H_CUERVO_
#define __CLOG_H_CUERVO_

#include <string.h>
#include <stdio.h>
#include <stdarg.h>

#include <time.h>
#include <fstream>
#include <ios>

class CLog
{
    public:
        CLog();
        CLog(char* LogName);
        ~CLog() {};

    public:
        void AddString(char *error, ...);
        void AddLine(char *text);
        void AddMessage( char* str, ... );
        void AddTimeDate();
        void AddOSVersion();
        void AddMemoryStatus();

    private:
        char m_LogName[20];
};

#endif //__CLOG_H_CUERVO_