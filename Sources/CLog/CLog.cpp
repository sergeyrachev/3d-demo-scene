#include "CLog.h"
#include <windows.h>

using namespace std;

CLog::CLog()
{
    fstream *handler;
    char line[1024];

    handler = new fstream;

    handler->open( "Log.txt", ios::binary | ios::out | ios::trunc );

    if(handler->fail())
    {
        handler->close();
        return;
    }

    //Clean garbage
    memset( line,0,1024 );

    sprintf( (char*)m_LogName, "%s", "Log.txt" );

    handler->close();    

    AddLine("\n\nWelcome to our engine!\n");
	AddLine("----------------------\n\n\n");
    AddMemoryStatus();
    AddLine("\n");
    AddOSVersion();
    AddLine("\n");
    AddMessage("Log was started");
}

CLog::CLog( char *LogName )
{
    fstream *handler;
    char line[1024];

    handler = new fstream;

    handler->open( LogName, ios::binary | ios::out | ios::trunc );

    if(handler->fail())
    {
        handler->close();
        return;
    }

    sprintf( (char*)m_LogName, "%s", LogName ); 
    
    //Clean garbage
    memset( line,0,1024 );

    handler->close();

    AddLine("\n\nWelcome to our engine!\n");
	AddLine("----------------------\n\n\n");
    AddMemoryStatus();
    AddLine("\n");
    AddOSVersion();
    AddLine("\n");
    AddMessage("Log was started");
}

void CLog::AddTimeDate()
{
    char dbuffer[9];
    char tbuffer[9];

    _strdate(dbuffer);
    _strtime(tbuffer);

    AddString( "[%s %s] ", tbuffer, dbuffer );
}

void CLog::AddString( char *error, ... )
{
    va_list argptr;
    char    text[1024];
    char    text2[1024];

    va_start( argptr, error );
    vsprintf( text, error, argptr );
    va_end(argptr);

    sprintf( text2, "%s", text );

    AddLine(text2);
    OutputDebugString(text2);
}

void CLog::AddLine( char* text )
{
    fstream *handler;
    char line[1024];

    handler = new fstream;

    handler->open( (char*)m_LogName, ios::binary | ios::out | ios::app );

    if( handler->fail() )
    {
        handler->close();
        return;
    }

    memset( line, 0, 1024 );
    sprintf( line, "%s", text );

    handler->write( (char*)line, (std::streamsize)strlen(line) );

    handler->close();
}

void CLog::AddMessage( char* str, ... )
{
    va_list argptr;

    AddTimeDate();

    va_start( argptr, str );
    AddString( str, argptr );
    va_end(argptr);
    AddLine("\n");
}

void CLog::AddOSVersion()
{
    OSVERSIONINFO OSinfo;
    OSinfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

    GetVersionEx(&OSinfo);
    
    BOOL bIsWindows98orLater = ( OSinfo.dwPlatformId == VER_PLATFORM_WIN32_WINDOWS) &&
                               ( (OSinfo.dwMajorVersion > 4) ||
                               ( (OSinfo.dwMajorVersion == 4) && (OSinfo.dwMinorVersion > 0) ) );
    
    AddLine(".: OS Version Info :.\n");
    AddString( "Version: %u.%u.%u\n", OSinfo.dwMajorVersion,
              OSinfo.dwMinorVersion, OSinfo.dwBuildNumber & 0xFFFF );
    if( strlen(&OSinfo.szCSDVersion[0]) > 1 )
        AddString( "Additional info: %s\n", OSinfo.szCSDVersion );
    else
        AddLine( "Additional info: -none-\n" );
    
    switch(OSinfo.dwPlatformId)
    {
        case VER_PLATFORM_WIN32s:
            AddLine("Win32s on Windows 3.1 detected.\n");
            break;
        case VER_PLATFORM_WIN32_WINDOWS:
            if(bIsWindows98orLater)
                AddLine("Windows 98 or later detected.\n");
            else
                if(LOWORD(OSinfo.dwBuildNumber) > 1080)
                    AddLine("Windows 95 OSR2 detected.\n");
                else 
                    AddLine("Windows 95 detected.\n");
            break;
        case VER_PLATFORM_WIN32_NT:
            AddLine("Windows NT detected.\n");
        break;
    }

    AddLine(".: End OS Version Info :.\n");
}

void CLog::AddMemoryStatus()
{
    MEMORYSTATUS memstat;
    memstat.dwLength = sizeof(MEMORYSTATUS);

    GlobalMemoryStatus(&memstat);

    AddLine(".: Memory Stat :.\n");
    AddString("Total Physical Memory:     %.02f MB.\n", memstat.dwTotalPhys/1048576.f);
    AddString("Available Physical Memory: %.02f MB.\n", memstat.dwAvailPhys/1048576.f);
    AddString("Total Virtual Memory:      %u MB.\n", memstat.dwTotalVirtual/1048576);
    AddString("Available Virtual Memory:  %u MB.\n", memstat.dwAvailVirtual/1048576);
    AddLine(".: End Memory Stat :.\n");
}